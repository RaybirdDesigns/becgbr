<?php
/**
 * Template Name: Home
 */

$theme_colour = get_field('theme_colour');

?>

<?php get_header()?>

<style>
	
	.btn {
		border: solid 3px #<?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: #<?php echo $theme_colour ?>!important;
	}

	a,
	a:hover {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px #<?php echo $theme_colour ?>;
	}

	h2:before {
		border-bottom: 3px solid #<?php echo $theme_colour ?>!important;
	}

</style>

<?php if( have_rows('all_components') ): ?>

    <?php while ( have_rows('all_components') ) : the_row(); ?>

    	<?php if( get_row_layout() == 'standard_hero' ): ?>

        	<?php get_template_part('components/hero-banners/standard-hero/standard-hero', null) ?>

        <?php elseif( get_row_layout() == 'icon_nav' ): ?>

        	<?php include(locate_template('/components/icon-nav/icon-nav.php')); ?>

        <?php elseif( get_row_layout() == 'banner_ad' ): ?>
			
			<section id="banner-ad" class="collapse">
				<div class="small-12">
				<?php

					$post_object = get_field('banner_ad');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post );
					
						get_template_part('components/banner-ad/banner', 'ad');
								
						wp_reset_postdata();
					}
				?>
				</div>
			</section>

        <?php elseif( get_row_layout() == 'articles' ): ?>

			<?php get_template_part('components/articles/articles', null) ?>

        <?php elseif( get_row_layout() == 'events' ): ?>

			<?php get_template_part('components/events/events', null) ?>

        <?php elseif( get_row_layout() == 'grid_carousel' ): ?>

        	<?php include(locate_template('/components/image-text-grid-carousel/image-text-grid-carousel.php')); ?>

    	<?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>


<?php get_footer()?>

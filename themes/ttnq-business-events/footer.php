<?php

// Footer
$global_theme_colour = get_field('global_colour_theme', 'options');

?>

		</div>
		<style>

			#footer .social-link,
			#footer .back-to-top-btn {
				border: solid 2px <?php echo $global_theme_colour ?>!important;
			}

			#footer .social-link:hover,
			#footer .back-to-top-btn:hover {
				background-color: <?php echo $global_theme_colour ?>!important;
				border: solid 2px <?php echo $global_theme_colour ?>!important;
			}

			#footer.footer .menu li:first-child a,
			#footer .footer .menu li:first-child a:hover,
			#footer h5,
			#footer .back-to-top-wrapper p {
				color: <?php echo $global_theme_colour ?>!important;
			}

			#footer .footer-social h5 {
				color: #000!important;
			}

			#footer #menu-footer-legal-menu a {
				color: <?php echo $global_theme_colour ?>!important;
			}

			#footer.footer .footer-logo {
				fill: <?php echo $global_theme_colour ?>;
			}

		</style>
		<footer id="footer" class="footer">
			<div class="row">
				<div class="small-12 columns">
					<div class="back-to-top-wrapper">
						<p>Back to top</p> <button class="back-to-top-btn small-icon-btn"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
					</div>
					<div class="footer-social">
						<h5>Follow us</h5>
						<?php if( have_rows('social_links', 'option') ): ?>

							<ul class="list-reset list-inline footer-social-list">

							<?php while( have_rows('social_links', 'option') ): the_row(); 

								$social_channel = get_sub_field('social_channel');
								$social_channel_url = get_sub_field('social_url');

								?>

								<li><a href="<?php echo $social_channel_url; ?>" target="_blank" class="social-link"><i class="fa fa-<?php echo $social_channel; ?>" aria-hidden="true"></i></a>
								</li>

							<?php endwhile; ?>

							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row footer-menus small-up-1 medium-up-3 large-up-5">
				<div class="column">
					<h5 class="menu-item-footer-mobile">Our Region</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'our-region-menu' ) ); ?>
				</div>
				<div class="column">
					<h5 class="menu-item-footer-mobile">Resources</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'resources-menu' ) ); ?>
				</div>
				<div class="column">
					<h5 class="menu-item-footer-mobile">Suppliers</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'suppliers-menu' ) ); ?>
				</div>
				<div class="column">
					<h5 class="menu-item-footer-mobile">Media</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'media-menu' ) ); ?>
				</div>
				<div class="column">
					<h5 class="menu-item-footer-mobile">Events</h5>
					<?php wp_nav_menu( array( 'theme_location' => 'events-menu' ) ); ?>
				</div>
				<div class="column">
                    <a href="/contact-us" style="text-decoration: none; border-bottom-width: 0;">
                        <h5 class="menu-item-footer-mobile">Contact Us</h5>
                    </a>
				</div>
			</div>
			<div class="row">
				<div class="small-12 large-2 columns">
					<svg class="footer-logo" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tnq-logo"></use>
                        </svg>
				</div>
				<div class="small-12 large-10 columns">
					<div class="footer-legal">
						<div class="footer-legal-menu">
							<?php wp_nav_menu( array( 'theme_location' => 'footer-legal-menu' ) ); ?>
						</div>
						<div>
							<p class="footer-copyright">© <span><?php echo date("Y"); ?></span> <?php the_field('footer_legal', 'options'); ?> </p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<script>

			class ResponsiveBackgroundImage {

			    constructor(element) {
			        this.element = element;
			        this.img = element.querySelector('img');
			        this.src = '';

			        this.img.addEventListener('load', () => {
			            this.update();
			        });

			        if (this.img.complete) {
			            this.update();
			        }
			    }

			    update() {
			        var src = typeof this.img.currentSrc !== 'undefined' ? this.img.currentSrc : this.img.src;
			        if (this.src !== src) {
			            this.src = src;
			            this.element.style.backgroundImage = 'url("' + this.src + '")';

			        }
			    }
			}

			// run responsive bg on all elements with data attribute
			var bgImageElements = document.querySelectorAll('[data-responsive-background-image]');

			if (bgImageElements.length >= 1 ) {
				for (var i=0; i<bgImageElements.length; i++) {
					new ResponsiveBackgroundImage(bgImageElements[i]);
				}
			}

		</script>
		<?php wp_footer()?>
    </body>
</html>
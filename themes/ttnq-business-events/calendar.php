<?php
/**
 * Template Name: Calendar
 */

$theme_colour = get_field('global_colour_theme', 'options');

?>

<?php get_header(); ?>

<style>

	h2:before {
		border-bottom: solid 3px <?php echo trim($theme_colour) ?>!important;
	}

	.dates-list li a {
		color: <?php echo trim($theme_colour) ?>!important;
	}

	.dates-list li a:hover,
	.dates-list li.active a {
		border-bottom: solid 2px <?php echo trim($theme_colour) ?>!important;
	}

	ul.pagination .next a,
	ul.pagination .previous a {
		border: solid 3px <?php echo trim($theme_colour) ?>!important;
	}

	ul.pagination .next a:hover,
	ul.pagination .previous a:hover {
		border: solid 3px <?php echo trim($theme_colour) ?>!important;
		background-color: <?php echo trim($theme_colour) ?>!important;
	}
	
</style>

<?php get_template_part('components/hero-banners/short-hero/short-hero', null) ?>

<?php if (have_posts()) : ?>
<section id="calendar-header" class="calendar-header">
	<div class="row">
		<div class="small-12 large-8 large-offset-2 columns text-center">
			<h1><?php the_title(); ?></h1>
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
</section>
<?php endif; ?>

<div id="calendar-root"
	 data-month="<?php echo date('M'); ?>"
	 data-year="<?php echo date('Y'); ?>"
	 data-posts-per-page="<?php the_field('posts_per_page'); ?>"
	 data-api-url="<?php the_field('api_base_url'); ?>"
	 data-theme-colour="<?php the_field('global_colour_theme_backgrounds', 'options'); ?>"></div>

<?php get_footer(); ?>
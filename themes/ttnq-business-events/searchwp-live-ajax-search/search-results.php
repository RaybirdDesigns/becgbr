<?php
/**
 * Search results are contained within a div.searchwp-live-search-results
 * which you can style accordingly as you would any other element on your site
 *
 * Some base styles are output in wp_footer that do nothing but position the
 * results container and apply a default transition, you can disable that by
 * adding the following to your theme's functions.php:
 *
 * add_filter( 'searchwp_live_search_base_styles', '__return_false' );
 *
 * There is a separate stylesheet that is also enqueued that applies the default
 * results theme (the visual styles) but you can disable that too by adding
 * the following to your theme's functions.php:
 *
 * wp_dequeue_style( 'searchwp-live-search' );
 *
 * You can use ~/searchwp-live-search/assets/styles/style.css as a guide to customize
 */
$search_query = isset( $_POST['swpquery'] ) ? sanitize_text_field( $_POST['swpquery'] ) : '';
$full_results_link = add_query_arg(
  array(
    's' => $search_query
  ),
  site_url()
);

?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php $post_type = get_post_type_object( get_post_type() ); ?>
		<div class="searchwp-live-search-result">

			<?php
			// get hero image thumbnail
			$feaure_image_id = get_post_thumbnail_id($post->ID);
			$acf_image_id = wp_get_attachment_image_url(get_field('hero_banner_image'), 'thumbnail' );

			if($feaure_image_id) {
				$image_src = wp_get_attachment_image_url($feaure_image_id, 'thumbnail');
			} else if ($acf_image_id)  {
				$image_src = $acf_image_id;
			} else {
				$image_src = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'thumbnail')[0];
			}

			// get post type
			$post_type = get_post_type();

			switch( $post_type ) {
				    case 'post':
				         $post_type_display = 'Article';
				    break;
				    case 'accommodation':
				         $post_type_display = 'Accommodation';
				    break;
				    case 'event':
				         $post_type_display = 'Event';
				    break;
				    case 'product':
				         $post_type_display = 'Product';
				    break;
				    default:
				    	$post_type_display = null;
				}

			//get any tags
			$allposttags = get_the_tags();
			$first_tag = $allposttags[0]->name;
			$second_tag = $allposttags[1]->name;

			//remove excerpt filter for search results
			remove_filter('excerpt_more', 'new_excerpt_more');

			// get and transform caption text.
			$text = get_the_excerpt() ? wp_trim_words( get_the_excerpt(), 15, null ) : wp_trim_words(wp_strip_all_tags(get_field('hero_description')), 15, null );
			// exclude if event post type.
			$non_event_text = $post_type == 'event' ? null : $text;

			// get experience icon
			$experience_icon = get_field('icon');

			// get category location
			$cat = get_the_category();
			$first_cat = $cat[0]->cat_name != 'Uncategorised' ? $cat[0]->cat_name : null;

			// get event date
			if ($post_type == 'event') {

				// get date to split up
				$date = strtotime(get_field('time_of_event'));
				$date_end = strtotime(get_field('end_time_of_event'));

				// get date type
				$date_type = get_field('date_type');

				// show the start date month if different from end date month
				$start_month = date('F', $date) !== date('F', $date_end) ? date('F', $date) : null;

				// create formatted date
				if ($date_type == 'single-date') {
					$event_date_display = get_field('time_of_event');
				} else if ($date_type == 'date-range') {
					$event_date_display = date('j', $date).' '.$start_month.' - '.date('j', $date_end).' '.date('F', $date_end).' '.date('Y', $date_end);
				} else {
					$event_date_display = date('F', $date).' '.date('Y', $date);
				}
			}

			// add_filter('the_title', 'highlight_results');

			?>

			<div class="result-item row">
				<div class="result-item-image" style="background-image: url(<?php echo $image_src; ?>);"></div>
				<div class="result-item-text">
					<a href="<?php echo esc_url( get_permalink() ); ?>">

						<?php if($post_type == 'event' && get_field('time_of_event')): ?>
						<p class="result-item-date"><?php echo $event_date_display; ?></p>
						<?php endif; ?>

						<h3><?php the_title(); ?></h3>
						<p class="result-item-para"><?php echo $non_event_text; ?></p>

						<?php if(!empty($first_cat)): ?>
						<p class="result-item-terms"><?php echo $first_cat; ?></p>
						<?php endif; ?>

						<?php if(!empty($experience_icon)): ?>
						<p class="result-item-terms terms-icon">
							<svg class="result-item-exp-icon" role="presentation">
								<use xlink:href="<?php echo get_template_directory_uri(); ?>/images/site-sprite.svg#<?php echo $experience_icon; ?>"></use>
				            </svg> experience
						</p>
						<?php endif; ?>

						<?php if(!empty($post_type_display)): ?>
						<p class="result-item-terms"><?php echo $post_type_display; ?></p>
						<?php endif; ?>

						<?php if($first_tag && $first_tag != 'blank'): ?>
						<p class="result-item-terms"><?php echo $first_tag; ?></p>
						<?php endif; ?>

						<?php if($second_tag && $second_tag != 'blank'): ?>
						<p class="result-item-terms"><?php echo $second_tag; ?></p>
						<?php endif; ?>

					</a>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
		<div class="show-all-results row">
			<a href="<?php echo esc_url( $full_results_link ); ?>">Show all results</a>
		</div>
<?php else : ?>
	<div class="searchwp-live-search-no-results row column text-center">
		<p>
			<em><?php _ex( 'No results found.', 'swplas' ); ?></em>
		</p>
	</div>
<?php endif; ?>

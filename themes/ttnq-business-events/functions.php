<?php

// flush_rewrite_rules();


/*
	=======================================
	Add Theme Support
	=======================================
*/
add_theme_support( 'title-tag' );

/*
	=======================================
	Insert Custom Login Logo
	=======================================
*/
function custom_login_logo() {
	echo '
		<style>
			.login h1 a { background-image: url(' . get_template_directory_uri() . '/images/logos/logo-login.png) !important; background-size: 320px 240px; width:320px; height:240px; display:block; }
		</style>
	';
}
add_action( 'login_head', 'custom_login_logo' );


/*
	=======================================
	Set base url
	=======================================
	In development on local host the base url should be set
	to localhost:8080 so the webpack dev server can 
*/
function base_url(){
	$url = $_SERVER['SERVER_NAME'];

	$base_url = strpos($url, 'local') ? 'http://localhost:8080' : get_template_directory_uri();

	return $base_url;
}

/*
	=======================================
	Add scripts and css
	=======================================
*/
function add_theme_scripts() {

	wp_enqueue_script( 'main', base_url() . get_hashed_asset('main.js'), false, 1.1, true);

	if (is_single()) {
		wp_enqueue_script( 'product', base_url() . get_hashed_asset('product.js'), false, 1.1, true);
	}

	if (is_search()) {
		wp_enqueue_script( 'searchListingApp', base_url() . get_hashed_asset('searchListingApp.js'), false, 1.1, true);
	}

	if (is_page_template('itineraries.php')) {
		wp_enqueue_script( 'itineraries', base_url() . get_hashed_asset('itineraries.js'), false, 1.1, true);
	}

	if (is_page_template('calendar.php')) {
		wp_enqueue_script( 'calendarApp', base_url() . get_hashed_asset('calendarApp.js'), false, 1.1, true);
		// wp_localize_script( 'calendarApp', 'categories_filter_calendar', calendar_filters() );
	}

	if (base_url() != 'http://localhost:8080') {
		wp_enqueue_style( 'main', get_template_directory_uri() . get_hashed_asset('main.css') );
		wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/style.css' );
	}

	wp_localize_script( 'main', 'ajax_list_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );


}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/*
	=======================================
	Limit excerpt length
	=======================================
*/
function custom_excerpt_length( $length ) {
        return 14;
	}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_trim_excerpt($text) { // Fakes an excerpt if needed
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$text = strip_tags($text);
		$excerpt_length = 1;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			array_push($words, '[...]');
			$text = implode(' ', $words);
		}
	}
	return $text;
}
add_filter('get_the_excerpt', 'custom_trim_excerpt');


/*
	=======================================
	Add tag support to pages
	=======================================
*/
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

/*
	=======================================
	Assign product template to custom post types
	=======================================
*/
add_filter( 'single_template', function( $template ) {

    $my_types = array( 'accommodation', 'activities', 'professional_support', 'conference_venue', 'dining_restaurants', 'offsite_venues');
    $post_type = get_post_type();

    if ( ! in_array( $post_type, $my_types ) )
        return $template;

    return get_stylesheet_directory() . '/single-product.php';
});



/*
	=======================================
	Ensure all tags are included in queries
	=======================================
*/
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}



/*
	=======================================
	Add categories to pages
	=======================================
*/
function categories_for_pages(){
    register_taxonomy_for_object_type( 'category', 'page' );
}
add_action( 'init', 'categories_for_pages' );



/*
	=======================================
	Includes
	=======================================
*/

require get_template_directory() . '/includes/menus.php';
require get_template_directory() . '/includes/image-helpers.php';
require get_template_directory() . '/includes/helpers.php';
require get_template_directory() . '/includes/acf-setup.php';
require get_template_directory() . '/includes/remove-stuff.php';
require get_template_directory() . '/includes/post-types.php';
require get_template_directory() . '/includes/taxonomies.php';
require get_template_directory() . '/includes/priority-sort.php';
require get_template_directory() . '/includes/results-api.php';
require get_template_directory() . '/includes/custom-search-end-points.php';
require get_template_directory() . '/includes/list-ajax.php';
require get_template_directory() . '/includes/searchwp-live-ajax-search.php';
require get_template_directory() . '/includes/calendar-helper.php';
require get_template_directory() . '/includes/asset-management.php';


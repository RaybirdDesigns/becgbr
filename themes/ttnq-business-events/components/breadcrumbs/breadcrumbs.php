<?php

$current_post_type = get_post_type();
if ($current_post_type) {
	$post_type_label = get_post_type_object( $current_post_type )->label;
}

// used to get the parent of a page
$ancestors = get_post_ancestors( $post );
$ancestor_count = count($ancestors);

?>

<div class="breadcrumb">

	<ul class="list-reset list-inline breadcrumb-list">
		<li><a href="<?php echo site_url(); ?>">Home</a></li>

		<?php if ($current_post_type == 'post' && !is_search()) : ?>
		<li><a href="<?php echo get_home_url().'/articles' ?>">Articles</a></li>
		<?php endif; ?>

		<?php if ($current_post_type == 'itinerary' && !is_search()) : ?>
		<li><a href="<?php echo get_home_url().'/resources/sample-itineraries' ?>">Itineraries</a></li>
		<?php endif; ?>
		
		<?php if ($ancestor_count >= 3  && !is_search()) : ?>
		<li><a href="<?php echo get_the_permalink( $ancestors[2], false ); ?>"><?php echo get_the_title( $ancestors[2] ); ?></a></li>
		<?php endif; ?>

		<?php if ($ancestor_count >= 2  && !is_search()) : ?>
		<li><a href="<?php echo get_the_permalink( $ancestors[1], false ); ?>"><?php echo get_the_title( $ancestors[1] ); ?></a></li>
		<?php endif; ?>

		<?php if ( $post && $post->post_parent && !is_search()):  ?>
		<li>
	    <a href="<?php echo get_permalink( $post->post_parent ); ?>" >
	    <?php echo get_the_title( $post->post_parent ); ?>
	    </a>
	   </li>
		<?php endif; ?>

		<?php if (is_archive()) : ?>
		<li class="current-page">All <?php echo $post_type_label; ?></li>
		<?php endif; ?>

		<?php if (!is_search() && !is_archive()) : ?>
		<li class="current-page"><?php the_title(); ?></li>
		<?php endif; ?>

		<?php if (is_search()) : ?>
		<li class="current-page">search results</li>
		<?php endif; ?>


	</ul>
</div>
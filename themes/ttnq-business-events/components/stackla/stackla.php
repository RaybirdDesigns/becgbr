<section class="stackla">
	<div class="row">
		<div class="small-12 large-8 large-offset-2 columns">
			<h2><?php the_sub_field('heading_sk'); ?></h2>
		</div>
	</div>
	<div class="clearfix">
		<?php the_sub_field('stackla_snippet_sk'); ?>
	</div>
	<?php get_template_part('components/button/button', null); ?>
</section>

<?php the_field('stackla_snippet', 'option'); ?>
<?php

$distance_day = get_sub_field('distance'); 
$time_day = get_sub_field('time'); 
$heading_day = get_sub_field('heading'); 
$sub_heading_day = get_sub_field('sub_heading'); 
$text_link_day = get_sub_field('link_text') ? get_sub_field('link_text') : 'Go to Google Maps'; 
$url_link_day = get_sub_field('link_url');

?>

<section id="day-<?php echo $day_index; ?>" class="itinerary-section">
	<div class="row">
		<div class="day-top columns">
			<svg class="itinerary-icon"  role="presentation">
				<use xlink:href="<?php echo get_template_directory_uri()?>/images/sprites/blog-sprite.svg#<?php echo $transport['value']; ?>"></use>
            </svg>
            <div class="itinerary-distance">
	        	<span class="itinerary-distance-left"><?php echo $distance_day; ?></span>
				<span class="itinerary-distance-right"><?php echo $time_day; ?></span>
	        </div>
			<div class="itinerary-line">
	        	<svg id="dashed-line">
					<line stroke-dasharray="22, 8" x1="0" y1="0" x2="0" y2="100%"></line>
				</svg>
	        </div>
	        <?php if(get_sub_field('day_override')): ?>
	        	<h3><?php the_sub_field('day_override'); ?></h3>
	        <?php else: ?>
			<h3><?php echo 'Day '.$day_index; ?></h3>
			<?php endif; ?>
			<h2><?php echo $heading_day; ?></h2>
			<?php if ($sub_heading_day): ?>
			<p><?php echo $sub_heading_day; ?></p>
			<?php endif; ?>
			<?php if ($url_link_day): ?>
				<a target="_blank" href="<?php echo $url_link_day; ?>"><?php echo $text_link_day; ?></a>
			<?php endif; ?>
		</div>
	</div>
	<div class="sites-wrapper">
		<div class="itinerary-line">
        	<svg id="dashed-line">
				<line stroke-dasharray="22, 8" x1="0" y1="0" x2="0" y2="100%"></line>
			</svg>
        </div>
		<?php if( have_rows('points_of_interest') ): ?>
			<?php $index = 1; ?>
			<?php while ( have_rows('points_of_interest') ) : the_row(); ?>

				<?php
				$index++;
				$site_image = get_sub_field('image');
				$site_image_size = get_sub_field('image_size');
				$site_image_res = $site_image_size == 'large' ? 'full' : 'medium';
				$site_icon = get_sub_field('icon');
				$site_text = get_sub_field('text');
				$site_alignment = get_sub_field('alignment');
				$site_text_position = get_sub_field('text_position') == 'above' ? 'text-above' : null;
				$left_right = $day_index & 1 ? ['right','left'] : ['left','right'];
				$site_direction = $index & 1 ? 'sites-content-'.$left_right[0] : 'sites-content-'.$left_right[1];
				$use_direction = $site_image_size != 'large' && $site_alignment == 'default' ? $site_direction : 'sites-content-'.$site_alignment;
				?>
		
			    <div class="row sites-content <?php echo $use_direction; ?> <?php echo 'image-size-'.$site_image_size; ?> <?php echo $site_text_position; ?>">
			    	<div class="sites-content-image">
			    	<?php if($site_image): ?>
			    		<img class="site-image" <?php echo responsive_image($site_image, $site_image_res); ?> alt="<?php echo get_post_meta($site_image)['_wp_attachment_image_alt'][0]; ?>">
			    	<?php endif; ?>
			    	</div>
			    	<div class="sites-content-icon">
			    		<?php if(get_sub_field('icon')): ?>
						<?php echo file_get_contents(htmlspecialchars($site_icon), false, null); ?>
						<?php endif; ?>
			    	</div>
			    	<div class="sites-content-text">
			    		<?php echo $site_text; ?>
			    	</div>
			    </div>
		
			<?php endwhile; ?>
		<?php endif; ?>
		<div class="itinerary-distance">
        	<span class="itinerary-distance-left"><?php echo $distance_day; ?></span>
			<span class="itinerary-distance-right"><?php echo $time_day; ?></span>
        </div>
	</div>
	
</section>
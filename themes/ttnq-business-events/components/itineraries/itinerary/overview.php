<section class="itinerary-overview">
	<div class="row">
		<div class="quick-info">
			<?php if($distance): ?>
				<div class="quick-info-item small-6 medium-3 columns">
					<svg class="itinerary-icon quick-info-icon" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri()?>/images/sprites/blog-sprite.svg#distance"></use>
	                </svg>
	                <p>Distance<br/>
	                <strong><?php echo $distance; ?></strong></p>
				</div>
			<?php endif; ?>
			<?php if(get_field('mode_of_transport')): ?>
				<div class="quick-info-item small-6 medium-3 columns">
					<svg class="itinerary-icon quick-info-icon" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri()?>/images/sprites/blog-sprite.svg#<?php echo $transport['value']; ?>"></use>
	                </svg>
	                <p>How<br/>
	                <strong><?php echo $transport['label']; ?></strong></p>
				</div>
			<?php endif; ?>
			<?php if(get_field('location')): ?>
				<div class="quick-info-item small-6 medium-3 columns">
					<svg class="itinerary-icon quick-info-icon" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri()?>/images/sprites/blog-sprite.svg#where"></use>
	                </svg>
	                <p>Where<br/>
	                <strong><?php echo $location; ?></strong></p>
				</div>
			<?php endif; ?>
			<?php if(get_field('date')): ?>
				<div class="quick-info-item small-6 medium-3 columns">
					<svg class="itinerary-icon quick-info-icon" role="presentation">
						<use xlink:href="<?php echo get_template_directory_uri()?>/images/sprites/site-sprite.svg#listings-events"></use>
	                </svg>
	                <p>Date<br/>
	                <strong><?php echo $date; ?></strong></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if (have_posts()) : ?>
	<div class="row">
		<div class="overview-body columns">
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
	<?php endif; ?>
</section>


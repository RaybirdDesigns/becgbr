<?php if( get_sub_field('button_url_bn') || get_sub_field('button_link_bn') || get_field('button_url_bn') || get_field('button_link_bn') ):
	
	$button_url_field = get_sub_field('button_url_bn') ? get_sub_field('button_url_bn') : get_field('button_url_bn');
	$button_url = $button_url_field ? $button_url_field : get_sub_field('button_link_bn');
	$icon = $button_url_field ? 'external-link' : 'chevron-right';
	$target = $button_url_field ? 'target="_blank"' : null;
	$button_text = get_sub_field('button_text_bn') ? get_sub_field('button_text_bn') : get_field('button_text_bn');
?>

	<div class="btn-wrapper row columns"><a <?php echo $target; ?> class="gtc-border btn btn-medium btn-ghost" href="<?php echo $button_url; ?>"><?php echo $button_text; ?> <i class="fa fa-<?php echo $icon; ?>" aria-hidden="true"></i></a></div>

<?php endif; ?>
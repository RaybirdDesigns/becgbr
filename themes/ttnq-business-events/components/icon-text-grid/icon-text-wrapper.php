<?php $is_grey_bg_ict = get_sub_field('grey_background_ict') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_ict') == 'white' ? 'white-background' : null); ?>

<section class="icon-text collapse-bottom <?php echo $is_grey_bg_ict; ?>">
	<?php if(get_sub_field('heading_ict')): ?>
	<div class="row component">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php the_sub_field('heading_ict'); ?>
		</div>
	</div>
	<?php endif; ?>
	<div class="row">
		<ul class="list-reset icon-text-list">
			<?php if(have_rows('icon_and_text_ict')): ?>
			<?php while ( have_rows('icon_and_text_ict') ) : the_row(); ?>
				<li class="small-12 large-6 columns">
					<?php get_template_part('components/icon-text-grid/icon-text', null); ?>
				</li>
			<?php endwhile; endif; ?>
		</ul>
	</div>
</section>
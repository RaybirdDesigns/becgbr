<div class="icon-text-tile clearfix">
	<div class="icon-text-icon-wrapper small-12 medium-2 float-left">
		<?php if(get_sub_field('icon')): ?>
		<?php echo file_get_contents(htmlspecialchars(get_sub_field('icon'))); ?>
		<?php endif; ?>
	</div>
	<div class="icon-text-text small-12 medium-10 float-left">
		<?php the_sub_field('text'); ?>
	</div>
</div>
<?php

$tile_index = 0;

echo '<ul class="tile-4-list clearfix">';
$i = 0;
foreach( $posts as $post ):
	setup_postdata( $post );
		$tile_index++;
		echo '<li class="small-6 medium-6 large-3 columns">';
				if (is_user_logged_in() && $tile_index <= $selected_post_count) {
					echo '<div class="authored">Authored</div>';
				}
			include(locate_template('components/custom-post-tile/custom-post-tile.php'));
		echo '</li>';
endforeach;
wp_reset_postdata();
echo '</ul>';
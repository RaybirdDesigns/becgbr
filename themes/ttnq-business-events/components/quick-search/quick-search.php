<?php

$post_type_slug_arr = get_sub_field('post_types_filter');
$categories = get_categories();

?>

<div class="quick-search">
	<div class="row text-center">
		<h2>Quick Search</h2>
	</div>
    <div class="row columns">
        <div class="quick-search-form">
			<form role="search" method="get" class="search-form clearfix" action="<?php echo get_site_url(); ?>">
				<div class="small-12 large-4 columns">
					<label class="show-for-sr" for="post_type">Select a Category</label>
					<select name="post_type" id="quick-search-category-select">
						<option value>Please Select a Category</option>
						<?php
							foreach ($post_type_slug_arr as $item) {
								echo '<option value="'.$item.'">'.get_post_type_object($item)->label.'</option>';
							}
						?>
					</select>
				</div>
				<div class="small-12 large-4 columns">
					<label class="show-for-sr" for="category">Select a Region</label>
					<select name="categoryID" id="quick-search-region-select">
						<option value>Please Select a Region</option>
						<?php
							foreach ($categories as $item) {
								if ($item->cat_ID != 1) {
									echo '<option value="'.$item->cat_ID.'">'.$item->name.'</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="small-12 large-4 columns">
					<label class="show-for-sr" for="s">Company Name</label>
					<input name="s" type="search" class="search-field" placeholder="Company Name" value="" title="Search for:" />
					<!-- <input name="qs" type="hidden" value="true"/> -->
				</div>
				<div class="text-center">
					<button class="btn btn-medium gtn-ghost" type="submit">Search</button>
				</div>
			</form>
        </div>
    </div>
</div>
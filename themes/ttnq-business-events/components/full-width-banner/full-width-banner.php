<?php

$fwb_url_internal = get_sub_field('full_width_bg_page_url_fwb') ? get_sub_field('full_width_bg_page_url_fwb') : get_field('full_width_bg_page_url_fwb');
$fwb_url_external = get_sub_field('full_width_bg_url_fwb') ? get_sub_field('full_width_bg_url_fwb') : get_field('full_width_bg_url_fwb');
$fwb_url = $fwb_url_internal ? $fwb_url_internal : $fwb_url_external;
$fwb_image = get_sub_field('full_width_bg_image_fwb') ? get_sub_field('full_width_bg_image_fwb') : get_field('full_width_bg_image_fwb');
$fwb_text = get_sub_field('full_width_bg_text_fwb') ? get_sub_field('full_width_bg_text_fwb') : get_field('full_width_bg_text_fwb');
$fwb_link_text = get_sub_field('full_width_bg_link_text_fwb') ? get_sub_field('full_width_bg_link_text_fwb') : get_field('full_width_bg_link_text_fwb');
$fwb_target = $fwb_url_internal ? null : 'target="_blank"';
?>

<section class="collapse full-width-banner text-center">
	<img <?php responsive_image($fwb_image, 'large') ?> >
	<div class="full-width-banner-content">
		<?php echo $fwb_text; ?>
		<?php if(!empty($fwb_url)): ?>
		<a <?php echo $fwb_target; ?> class="btn btn-medium btn-ghost btn-margin" href="<?php echo $fwb_url; ?>"><?php echo $fwb_link_text; ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		<?php endif; ?>
	</div>
</section>
<?php
// Events wrapper pulls in event tiles.
$event_type = get_sub_field('event_type_e');
$hide_heading = get_sub_field('hide_heading_e');
$event_padding = $hide_heading ? 'collapse-bottom' : 'collapse';
$is_grey_bg_e = get_sub_field('grey_background_e') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_e') == 'white' ? 'white-background' : null);

?>

<section id="events-<?php echo $event_type; ?>" class="<?php echo $event_padding; ?> <?php echo $is_grey_bg_e; ?>">
	<?php if($hide_heading == true): ?>
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center component">
			<?php the_sub_field('events_text_e'); ?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if($event_type == 'corp'): ?>
		<?php include(locate_template('/components/events/corp-events.php')); ?>
	<?php elseif($event_type == 'regional'): ?>
		<?php include(locate_template('/components/events/regional-events.php')); ?>
	<?php endif; ?>
</section>
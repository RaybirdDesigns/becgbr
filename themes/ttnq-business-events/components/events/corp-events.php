<?php
/*
	Set dynamic events tiles with custom override
*/

// set vars
$tile_spaces = 5;
$selected_post_count = 0;
$row_name = 'corporate_events_e';
$subfield_name = 'corporate_event';
$event_category = get_sub_field('corporate_category_e');

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_sub_field($row_name);

	// clean the acf array, remove the 'featured_events' key.
	if (is_array($selected_posts_obj)) {

		$clean_selected_posts_obj = array();
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// only run priority sort and merge if less than $tile_spaces
if ($selected_post_count < $tile_spaces) {

	// merge the two arrays together if there are selected tiles
	// otherwise just return dynamic posts
	if ($selected_post_count > 0) {

		// calculate number of pages needed from event priority sort query
		$sort_posts_per_page = $tile_spaces - $selected_post_count;

		// event priority sort query
		// priority_event_sort($posts_per_page, $paged, $category, $tag)
		$priority_wp_query = priority_event_sort($sort_posts_per_page, false, $event_category, null);
		$priority_wp_query_posts = $priority_wp_query->posts;

		$posts = array_merge($clean_selected_posts_obj, $priority_wp_query_posts);

	} else {

		// event priority sort query
		// priority_event_sort($posts_per_page, $paged, $category, $tag)
		$priority_wp_query = priority_event_sort(5, false, $event_category, null);
		$priority_wp_query_posts = $priority_wp_query->posts;

		$posts = $priority_wp_query_posts;
	}

} else {

	$posts = $clean_selected_posts_obj;
}

// echo '<pre>';
// print_r($posts);
// echo '</pre>';

?>

<?php if (is_user_logged_in() && !$posts): ?>
<!-- Notification if not enough posts found -->
<div class="tnq-notification">
	<p>The Events section doesn't have enough posts to be visible.</p>
</div>
<?php endif; ?>

<?php if ($posts): ?>
<div class="corp-events">
	<div class="row">
		<div class="smal-12 columns">
			<h3>Corporate Events</h3>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php include(locate_template('/components/tiles-5-up/tiles-5-up.php')); ?>
		</div>
	</div>

	<?php get_template_part('components/button/button', null); ?>

</div>
<?php wp_reset_query(); ?>
<?php endif; ?>
<?php

$acf_domain_name = get_sub_field('remote_domain_e');

// The following gets the featured image urls from the tnq db
$sql_posts = $ttnqDB->prepare(
	"SELECT
	wp_posts.*
	FROM wp_posts
	WHERE wp_posts.post_type = %s AND wp_posts.post_status = %s
	ORDER BY wp_posts.post_date DESC LIMIT 5",
	'event', 'publish'
);
$event_posts = $ttnqDB->get_results($sql_posts);

$start_time = [];
$event_type = [];
$full_image_paths = [];
foreach($event_posts as $event_post) {

	$event_post_id = $event_post->ID;

	// get image id
	$sql_thumb_id = $ttnqDB->prepare("SELECT meta_value FROM wp_postmeta WHERE meta_key=%s and post_id=%d", '_thumbnail_id', $event_post_id);

	// check if event post has a featrure image, if not use the global fallback
	if ($ttnqDB->get_results($sql_thumb_id)):

		$event_thumb_id = $ttnqDB->get_results($sql_thumb_id)[0]->meta_value;

		// use image id to get the image url
		$sql_thumb_url = $ttnqDB->prepare("SELECT guid FROM wp_posts WHERE post_type=%s and id=%d", 'attachment', $event_thumb_id);
		$event_thumb_url = $ttnqDB->get_results($sql_thumb_url)[0]->guid;

		// use image id to get image meta data
		$sql_thumb_medium_url = $ttnqDB->prepare("SELECT meta_value FROM wp_postmeta WHERE meta_key=%s and post_id=%d", '_wp_attachment_metadata', $event_thumb_id);
		$meta_value = $ttnqDB->get_results($sql_thumb_medium_url)[0]->meta_value;

		// get image file name from meta data
		$image_name = unserialize($meta_value)['file'];

		// get image file name with dimensions
		$image_name_medium = unserialize($meta_value)['sizes']['medium_large']['file'];

		// replace image file with image file name with dimensions
		$image_path_with_dimensions = str_replace($image_name,$image_name_medium,$event_thumb_url);

		// echo $image_path_with_dimensions;

		// if a domain name has been entered in acf field use it
		// otherwise use the path that was retrieved from the database
		if ($acf_domain_name) {
			// remove the domain from path from db
			$start = strpos($image_path_with_dimensions, '/wp-content');
			$image_path_with_dimensions_sans_domain = substr($image_path_with_dimensions, $start);

			// complete the url with custom domain
			$full_image_paths[] = $acf_domain_name.$image_path_with_dimensions_sans_domain;

		} else {
			$full_image_paths[] = $image_path_with_dimensions;
		}

	else :

		$full_image_paths[] = wp_get_attachment_image_url( get_field('global_fallback_image', 'options'), 'large' );

	endif;

	// -----------------get start time-----------------------

	$sql_start_time = $ttnqDB->prepare("SELECT * FROM wp_postmeta WHERE post_id = %d and meta_key = %s",$event_post_id, 'time_of_event');
	$start_time[] = $ttnqDB->get_results($sql_start_time)[0]->meta_value;

	// -----------------get event type-----------------------

	$sql_event_type = $ttnqDB->prepare("SELECT * FROM wp_postmeta WHERE post_id = %d and meta_key = %s",$event_post_id, 'event_category');
	$event_type[] = $ttnqDB->get_results($sql_event_type)[0]->meta_value;

};

?>

<div class="regional-events">
	<div class="row">
		<div class="smal-12 columns">
			<h3>Regional Events</h3>
		</div>
	</div>
	<div class="row small-collapse">
		<div class="small-12 columns">
			<?php

			$tile_index = 0;

			echo '<ul class="list-reset tiles-5-up">';
			$i = 0;
			foreach( $event_posts as $post ):
				setup_postdata( $post );
				$i++;
				$tile_index++;
				echo '<li class="small-6 medium-6 large-3">' ?>
					
					<?php

					// Custom post tile.
					// Shows feature image and a snippet of the content.
					$start_date_raw = new DateTime($start_time[$i-1]);
					$start_date = $start_date_raw->format('j M Y');

					$regional_link = get_permalink() ? str_replace(site_url(), $acf_domain_name, get_permalink()) : '';

					$tile_image = $full_image_paths[$i-1];

					$event_category = $event_type[$i-1];

					?>

					<a target="_blank" href="<?php echo $regional_link; ?>"
					   class="custom-post-tile"
					   style="background-image:url(<?php echo $tile_image; ?>);" >

					   <?php

					   $post_type_name = get_post_type();

					   // If post type is event use the acf field event_category
					    if ($event_category) {
							echo '<div class="custom-post-tile-tag">' . $event_category . '</div>';
						}

						?>

						<div class="custom-post-tile-text">
							<?php if ($start_date) : ?>
							<span class="custom-post-tile-text-date"><?php echo $start_date; ?></span>
							<?php endif; ?>
							<h5><?php the_title(); ?></h5>
							<?php if($i == 1 && has_excerpt()) : ?>
							<?php the_excerpt(); ?>
							<?php endif; ?>
							<div class="custom-post-tile-text-view">View <?php echo $post_type_name; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></div>
							<div class="overlay"></div>
						</div>
					</a>

				<?php echo '</li>';
			endforeach;
			wp_reset_postdata();
			echo '</ul>';

			?>
		</div>
	</div>
	<?php get_template_part('components/button/button', null); ?>
</div>
<?php 
$is_grey_bg_ac = get_sub_field('grey_background_ac') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_ac') == 'white' ? 'white-background' : null);

$articles_text = get_sub_field('articles_text_ac') ? get_sub_field('articles_text_ac') : get_field('articles_text_ac');

 ?>

<section id="articles" class="<?php echo $is_grey_bg_ac; ?>">
	<div class="row">
		<div class="small-12 medium-6 medium-offset-3 columns text-center">
			<?php echo $articles_text; ?>
		</div>
	</div>
	<div class="row">
		<div class="article-carousel">
		<?php if(have_rows('featured_articles_ac')) {

			while ( have_rows('featured_articles_ac') ) {

				the_row();

				$post_object = get_sub_field('featured_article');

				if ( $post_object ) {

					$post = $post_object;
					setup_postdata( $post_object );
					
					get_template_part('components/articles/article-tile');

					wp_reset_postdata();
				}
			}
		} ?>
		</div>
	</div>

	<div class="text-center">
		<?php get_template_part('components/button/button', null); ?>
	</div>
</section>
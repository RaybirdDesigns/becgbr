<?php

// Article tile.
// Shows feature image and a snippet of the content.

$carousel_image = get_field('carousel_image') ? wp_get_attachment_image_url(get_field('carousel_image'), 'large') : wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large" )[0];
$tile_image = $carousel_image ? $carousel_image : wp_get_attachment_image_src( get_field('global_fallback_image', 'options'), 'large' )[0];
$post_type = get_post_type();
// echo $post_type;
?>

<a href="<?php the_permalink(); ?>"
   class="article-tile" >

   <div class="article-tile-image" style="background-image: url('<?php echo $tile_image; ?>');"></div>
	
	<?php if (get_the_category( $id )[0]->slug !== 'uncategorized'): ?>
   <div class="article-tile-cat">
   		<?php echo get_the_category( $id )[0]->name; ?>
   </div>
   <?php endif; ?>
   <div class="article-tile-content">
   		<?php if ($post_type == 'post') : ?>
			<span class="article-tile-date"><?php the_date( 'j M Y', '', '', true ); ?></span>
		<?php endif; ?>
	   	<h5><?php the_title(); ?></h5>
	   	<div class="article-tile-content-desc">
	   		<?php the_excerpt(); ?>
			<div class="article-tile-content-desc-cta">
				Read more <i class="fa fa-angle-right" aria-hidden="true"></i>
			</div>
	   	</div>
   </div>
</a>
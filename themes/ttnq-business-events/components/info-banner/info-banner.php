<?php 

$hero_image_ib_ID = get_sub_field('background_image_ib') ? get_sub_field('background_image_ib'): get_field('background_image_ib');
// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if ($hero_image_ib_ID) {
	$full_res_desktop = wp_get_attachment_image_src($hero_image_ib_ID, 'full' )[0];
	$full_res_mobile = wp_get_attachment_image_src($hero_image_ib_ID, 'mobile-banner' )[0];
} else {
	$full_res_desktop = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'full')[0];
	$full_res_mobile = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'mobile-banner')[0];
}

 ?>

<style>
	.info-banner {
		background-image: url(<?php echo $full_res_desktop; ?>);
	}

	@media (max-width: 640px) {
		.info-banner {
			background-image: url(<?php echo $full_res_mobile; ?>);
		}
	}
</style>

<section id="info-banner" class="info-banner">
	<div class="row">
		<div class="info-banner-content">
			<h3><?php the_sub_field('heading_ib'); ?></h3>
			<?php the_sub_field('body_text_ib'); ?>
		</div>
	</div>
</section>
<?php
/*
	Set dynamic product tiles with custom override
*/
$is_grey_bg_field = get_sub_field('grey_background_pt') ? get_sub_field('grey_background_pt') : get_field('grey_background_pt');
$is_grey_bg_pt = $is_grey_bg_field == 'grey' ? 'grey-background' : ($is_grey_bg_field == 'white' ? 'white-background' : null);

$is_limited_to_4 = get_sub_field('limit_product_count_pt') || get_field('limit_product_count_pt') ? true : false;
$selected_post_type_field = get_sub_field('dynamic_product_type') ? get_sub_field('dynamic_product_type') : get_field('dynamic_product_type');
$selected_post_type = $selected_post_type_field ? $selected_post_type_field : null;

// set vars
$selected_post_count = 0;
$row_name = 'featured_products_pt';
$subfield_name = 'featured_product';

// count number of selected posts
if ( have_rows($row_name) ) {

	$selected_post_count = 0;

	while ( have_rows($row_name) ) {
		$selected_post_count++;
		the_row();
	}

	$selected_posts_obj = get_sub_field($row_name) ? get_sub_field($row_name) : get_field($row_name);

	// clean the acf array, remove the 'featured_events' key.
	if (is_array($selected_posts_obj)) {

		$clean_selected_posts_obj = array();
		foreach($selected_posts_obj as $item) {
			$clean_selected_posts_obj[] = $item[$subfield_name];
		}
	}
}

// merge the two arrays together if there are selected tiles
// otherwise just return dynamic posts
if ($selected_post_count > 0) {

	if ($selected_post_type !== null) {
		// priority_sort($posts_per_page, $paged, $post_type, $category, $tag)
		$priority_wp_query = priority_sort(-1, false, $selected_post_type, null, null);
		$priority_wp_query_posts = $priority_wp_query->posts;

		$posts = array_unique(array_merge($clean_selected_posts_obj, $priority_wp_query_posts), SORT_REGULAR);
	} else {
		$posts = $clean_selected_posts_obj;
	}

} else {

	// priority_sort($posts_per_page, $paged, $post_type, $category, $tag)
	$priority_wp_query = priority_sort(-1, false, $selected_post_type, null, null);
	$priority_wp_query_posts = $priority_wp_query->posts;

	$posts = $priority_wp_query_posts;
}

if ($is_limited_to_4) {
	$posts = array_slice($posts, 0, 4);
}

$product_title = get_sub_field('product_tiles_text_pt') ? get_sub_field('product_tiles_text_pt') : get_field('product_tiles_text_pt');

?>

<?php if ($selected_post_type !== null || $selected_post_count > 0): ?>
<style>
	p {
		margin-bottom: 18px!important;
	}
</style>

<section id="products-<?php echo generateRandomString(4); ?>" class="product-tiles show-more-section <?php echo $is_grey_bg_pt; ?>">
	<?php if($product_title): ?>
	<div class="row">
		<div class="small-12 medium-8 medium-offset-2 columns text-center">
			<?php echo $product_title; ?>
		</div>
	</div>
	<?php endif; ?>

	<div class="row show-more-container">
		<div class="small-12 columns show-more-container-inner">
			<?php include(locate_template('/components/tiles-4-up/tiles-4-up.php')); ?>
		</div>
	</div>

	<?php if (count($posts) > 8) : ?>
	<div class="row text-center">
		<button class="btn btn-medium btn-ghost btn-margin show-more-btn">See more</button>
	</div>
	<?php endif; ?>

</section>
<?php endif; ?>
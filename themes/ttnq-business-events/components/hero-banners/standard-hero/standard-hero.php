<?php

// Standard Hero Banner
$theme_colour = get_field('theme_colour') == 'global' ? get_field('global_colour_theme', 'options') : get_field('theme_colour');
$theme_bg_color = get_field('global_colour_theme_backgrounds', 'options');
$hero_image_sb_ID = get_sub_field('hero_image_sb');

$heading = get_sub_field('heading_text_sb') ? get_sub_field('heading_text_sb') : get_the_title();

$hide_breadcrumb_sb = get_sub_field('hide_breadcrumb_sb') ? get_sub_field('hide_breadcrumb_sb'): get_field('hide_breadcrumb_sb');
$white_border_sb = get_sub_field('white_border_sb') ? get_sub_field('white_border_sb'): get_field('white_border_sb');


// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if ($hero_image_sb_ID) {
	$thumb_nail_url = wp_get_attachment_image_src($hero_image_sb_ID, 'hero-thumb' )[0];
	$full_res_desktop = wp_get_attachment_image_src($hero_image_sb_ID, 'full' )[0];
	$full_res_mobile = wp_get_attachment_image_src($hero_image_sb_ID, 'mobile-banner' )[0];
} else {
	$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'hero-thumb')[0];
	$full_res_desktop = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'full')[0];
	$full_res_mobile = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'mobile-banner')[0];
}

?>

<style>
	
	.standard-hero:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

	.standard-hero {
		background-image: url(<?php echo $full_res_desktop; ?>);
	}

	@media (max-width: 640px) {
		.standard-hero {
			background-image: url(<?php echo $full_res_mobile; ?>);
		}
	}

	.theme-bg-colour {
		background-color: rgba(<?php echo hexToRGB($theme_bg_color); ?>, 0.95);
	}

	.standard-hero .btn {
		color: #ffffff;
	}

	h1:before {
		border-bottom: 3px solid #<?php echo $theme_colour ?>!important;
	}

</style>

<div id="hero-banner" class="standard-hero <?php echo !$hide_breadcrumb_sb ? 'has-breadcrumb' : null; ?>">
	<img class="hero-img" src="<?php echo $full_res_desktop; ?>" >
	<div class="standard-hero-content <?php echo $white_border_sb ? 'white-border' : 'theme-bg-colour'; ?>">
		<h1><?php echo $heading; ?></h1>
		<p><?php the_sub_field('heading_caption_sb'); ?></p>
		<?php get_template_part('components/button/button', null); ?>
	</div>
	<?php if(!$hide_breadcrumb_sb): ?>
		<?php include(locate_template('/components/breadcrumbs/breadcrumbs.php')); ?>
	<?php endif; ?>
</div>
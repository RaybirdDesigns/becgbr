<?php

// Standard Hero Banner
$theme_colour = get_field('theme_colour');
$hero_image_lb_ID = get_sub_field('background_image_lb') ? get_sub_field('background_image_lb'): get_field('background_image_lb');
$hide_breadcrumb_lb = get_sub_field('hide_breadcrumb_lb') ? get_sub_field('hide_breadcrumb_lb'): get_field('hide_breadcrumb_lb');

$category = get_the_category();
$product_type = get_field('product_type') ? get_field('product_type') : $category[0]->name;

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
	$thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "hero-thumb" )[0];
	$full_res_desktop = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" )[0];
	$full_res_mobile = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "mobile-banner" )[0];
} else {
	$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'hero-thumb')[0];
	$full_res_desktop = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'full')[0];
	$full_res_mobile = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'mobile-banner')[0];
}


?>

<style>
	
	.large-hero:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

	.large-hero {
		background-image: url(<?php echo $full_res_desktop; ?>);
	}

	@media (max-width: 640px) {
		.large-hero {
			background-image: url(<?php echo $full_res_mobile; ?>);
		}
	}

	.large-hero .btn {
		color: #ffffff;
	}

	h1:before {
		border-bottom: 3px solid #<?php echo $theme_colour ?>!important;
	}

</style>

<div id="hero-banner" class="large-hero">
	<img class="hero-img" src="<?php echo $full_res_desktop; ?>" >
	<div class="large-hero-content">
		<div class="large-hero-content-text">
			<?php if ($product_type): ?>
			<p class="category"><?php echo $product_type; ?></p>
			<?php endif; ?>
			<h1><?php the_title(); ?></h1>
			<?php 
				// get raw date string to use in data schema
				$raw_start_date = get_field('time_of_event', false, false);
				$raw_end_date = get_field('end_time_of_event', false, false);

				// get date to split up
				$date = strtotime(get_field('time_of_event'));
				$date_end = strtotime(get_field('end_time_of_event'));

				// get date type
				$date_type = get_field('date_type');

				// show the start date month if different from end date month
				$start_month = date('F', $date) !== date('F', $date_end) ? date('F', $date) : null;

				?>
				
				<?php if ($date_type == 'single-date') : ?>
				<p class="date" itemprop="startDate" content="<?php echo date('c', strtotime($raw_start_date)); ?>"><?php the_field('time_of_event'); ?></p>
				<?php elseif ($date_type == 'date-range') : ?>
				<p class="date">
					<span itemprop="startDate" content="<?php echo date('c', strtotime($raw_start_date)); ?>"><?php echo date('j', $date).' '.$start_month;?></span> - 
					<span itemprop="endDate" content="<?php echo date('c', strtotime($raw_end_date)); ?>"><?php echo date('j', $date_end).' '.date('F', $date_end).' '.date('Y', $date_end); ?></span>
				</p>
				<?php else : ?>
				<p class="date" itemprop="startDate" content="<?php echo date('Y', $date).'-'.date('m', $date).'-00'; ?>"><?php echo date('F', $date).' '.date('Y', $date);?></p>
				<?php endif; ?>
		</div>
	</div>
	<?php if (get_field('scroll_to_id')) : ?>
		<button class="btn scroll-down-btn scroll-down-btn-js" data-scroll-to="<?php the_field('scroll_to_id'); ?>">
			<svg class="down-arrow" role="presentation">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/sprites/global-sprite.svg#arrow-ico-white"></use>
            </svg>
		</button>
	<?php endif; ?>
	<?php if(!$hide_breadcrumb_lb): ?>
		<?php include(locate_template('/components/breadcrumbs/breadcrumbs.php')); ?>
	<?php endif; ?>
</div>
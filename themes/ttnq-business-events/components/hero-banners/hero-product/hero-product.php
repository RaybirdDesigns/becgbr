<?php

// Product hero banner

// process rating value to drive star rating
$star_rating = get_field('star_rating');
$rating_perc = $star_rating*20;

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
	$thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" )[0];
} else {
	$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}



?>

<style>
	
	.details-rating .star-wrapper span {
	    background-image: url(<?php echo get_template_directory_uri() ?>/images/icons/ico-stars.svg),none;
	}
	
	.hero-product:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

</style>

<div id="hero-banner" class="hero-product"
		 data-responsive-background-image >

	<?php if (has_post_thumbnail( $post->ID )) : ?>

        <img class="hero-img" <?php responsive_image_post('full') ?> >

    <?php else : ?>

       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />

    <?php endif; ?>

	<div class="row">
		<div class="hero-product-content columns">
			<div class="hero-product-content-text">
				<?php if (!empty(get_the_category())) : ?>
				<span class="hero-product-type"><?php echo get_the_category()[0]->name; ?></span>
				<?php endif; ?>
				<h1 itemprop="name"><?php the_title(); ?></h1>
				<ul class="hero-product-details hero-product-details-row-1 list-reset list-inline">
					<?php if(get_field('prod_email')) : ?>
					<li>
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<p class="details-title">Email</p>
						<a href="mailto:<?php the_field('prod_email'); ?>"><?php the_field('prod_email'); ?></a>
					</li>
					<?php endif; ?>
					<?php if(get_field('prod_phone')) : ?>
					<li>
						<i class="fa fa-phone" aria-hidden="true"></i>
						<p class="details-title">Phone</p>
						<p class="details-detail"><?php the_field('prod_phone'); ?></p>
					</li>
					<?php endif; ?>
					<?php if(get_field('prod_website')) : ?>
					<?php 
						$website_url = get_field('prod_website');
						$website_url = preg_replace('#^https?://#', '', $website_url);
					?>
					<li>
						<i class="fa fa-mouse-pointer" aria-hidden="true"></i>
						<p class="details-title">Website</p>
						<p class="details-detail"><a class="details-detail-website-link" target="_blank" href="<?php echo addhttp($website_url); ?>"><?php echo $website_url; ?></a></p>
					</li>
					<?php endif; ?>
					<?php if(get_field('star_rating')) : ?>
					<li>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<p class="details-title">Hotel Class</p>
						<div class="details-rating">
						    <strong class="show-for-sr">Rating <?php the_field('star_rating'); ?></strong>
						    <span class="star-wrapper"><span style="width:<?php echo $rating_perc; ?>%;"></span></span>
						</div>
					</li>
					<?php endif; ?>
				</ul>

				<?php if(get_field('prod_address_line_1') ||
						 get_field('prod_address_line_2') ||
						 get_field('prod_area') ||
						 get_field('prod_area') ||
						 get_field('url_facebook') ||
						 get_field('url_instagram') ||
						 get_field('url_youtube') ||
						 get_field('trip_advisor_id') ||
						 get_field('url_twitter')) : ?>
				<ul class="hero-product-details hero-product-details-row-2 list-reset list-inline">
					<?php if(get_field('prod_address_line_1') || get_field('prod_address_line_2') || get_field('prod_area') || get_field('prod_area'))  : ?>
					<li>
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<p class="details-title">Address</p>
						<p class="details-detail">
						<?php the_field('prod_address_line_1'); ?>
						<?php the_field('prod_address_line_2'); ?><br/>
						<?php echo $post_code = get_field('prod_city') ? get_field('prod_city') . "," : null; ?>
						<?php the_field('prod_suburb'); ?><br/>
						<?php the_field('prod_state'); ?>
						<?php echo $post_code = get_field('prod_postcode') ? get_field('prod_postcode') . "," : null; ?>
						<?php the_field('prod_country'); ?>
						</p>
					</li>
					<?php endif; ?>

					<?php if(get_field('trip_advisor_id')) : ?>
					<li>
						<i class="fa fa-tripadvisor" aria-hidden="true"></i>
						<p class="details-title">TripAdvisor Traveller Rating</p>
						<div id="trip-advisor-hero-rating"></div>
					</li>
					<?php endif; ?>

					<?php if(get_field('url_facebook') || get_field('url_instagram') || get_field('url_youtube') || get_field('url_twitter')) : ?>
					<li>
						<i class="fa fa-share" aria-hidden="true"></i>
						<p class="details-title">Social</p>
						<div class="details-social">
							<?php if(get_field('url_facebook')) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_facebook')); ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
							<?php endif; ?>
							<?php if(get_field('url_twitter')) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_twitter')); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<?php endif; ?>
							<?php if(get_field('url_instagram')) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_instagram')); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<?php endif; ?>
							<?php if(get_field('url_youtube')) : ?>
							<a class="details-social-link" target="_blank" href="<?php echo addhttp(get_field('url_youtube')); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
							<?php endif; ?>
						</div>
					</li>
					<?php endif; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<?php get_template_part('components/breadcrumb/breadcrumb'); ?>
	<div class="hero-bottom-gradient"></div>
</div>
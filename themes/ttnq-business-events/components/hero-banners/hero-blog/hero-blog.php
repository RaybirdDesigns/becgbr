<?php

// Blog hero banner

// get thumbnamil
// gets base64 encoded and used in the before pseudo element
if (has_post_thumbnail( $post->ID )) {
	$thumb_nail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" )[0];
} else {
	$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'medium')[0];
}

$category = get_the_category();
$product_type = $category[0]->name == 'Uncategorized' ? null : $category[0]->name;

?>

<style>
	.hero-blog:before {
		content: '';
		display: block;
		width: 100%;
		height:100%;
		position: absolute;
		top: 0;
		left: 0;
		background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
		filter: blur(10px);
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}
</style>

<div id="hero-banner" class="hero-blog" data-responsive-background-image>

	<?php if (has_post_thumbnail( $post->ID )) : ?>
        <img class="hero-img" <?php responsive_image_post('full') ?> >
    <?php else : ?>
       <img class="hero-img" <?php responsive_image(get_field('global_fallback_image', 'option'), 'full'); ?> />
    <?php endif; ?>

	<div class="row">
		<div class="hero-blog-content columns">
			<div class="hero-blog-content-text">
				<?php if($product_type): ?>
				<p><?php echo $product_type; ?></p>
				<?php endif; ?>
				<h1 itemprop="name"><?php the_title(); ?></h1>
				<p class="date" itemprop="datePublished" content="<?php echo get_the_date( 'Y-m-d', null, null, true ); ?>"><?php the_date( 'd F Y', null, null, true ); ?></p>
			</div>
		</div>
	</div>

	<?php get_template_part('components/breadcrumbs/breadcrumbs'); ?>
	<div class="hero-bottom-gradient"></div>
</div>
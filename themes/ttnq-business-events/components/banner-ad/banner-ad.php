<?php

/*
Description: Template for banner ad.
*/

$ad_url = addhttp(get_field('banner_ad_url'));
$target = get_field('banner_ad_target') ?  'target="_blank"' : null;

$img_alt_text = get_post_meta( get_field( 'banner_ad_image_desktop' ), '_wp_attachment_image_alt', true);

// get desktop srcset only
$image_srcset_desktop = wp_get_attachment_image_srcset( get_field( 'banner_ad_image_desktop' ), 'full' );
$image_srcset_mobile = wp_get_attachment_image_srcset( get_field( 'banner_ad_image_mobile' ), 'full' );

$ad_id = get_field('ad_id') ? 'data-adid="'.get_field('ad_id').'"' : null;

?>

<div class="banner-ad">
	<span class="banner-ad-tag">Advertisement</span>
	
	<?php if ($ad_url) : ?>
	<a class="banner-ad-link" href="<?php echo $ad_url; ?>" <?php echo $target; ?> <?php echo $ad_id; ?>>
	<?php endif; ?>
		<picture>
			<source media="(max-width: 640px)" srcset="<?php echo $image_srcset_mobile; ?>">
		  	<source media="(min-width: 641px)" srcset="<?php echo $image_srcset_desktop; ?>">
		  	<img src="<?php echo wp_get_attachment_image_url(get_field( 'banner_ad_image_desktop' ), 'full'); ?>" alt="<?php echo $img_alt_text; ?>">
		</picture>
	<?php if ($ad_url) : ?>
	</a>
	<?php endif; ?>
	
</div>
<div id="banner-ad-<?php echo generateRandomString(4); ?>">
	<div class="small-12">
	<?php

		$post_object = get_sub_field('banner_ad_ba');

		if ( $post_object ) {

			$post = $post_object;
			setup_postdata( $post );
		
			get_template_part('components/banner-ad/banner', 'ad');
					
			wp_reset_postdata();
		}
	?>
	</div>
</div>
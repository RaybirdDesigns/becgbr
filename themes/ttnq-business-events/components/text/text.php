<?php

$is_grey_bg_tx = get_sub_field('grey_background_tx') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_tx') == 'white' ? 'white-background' : null);
$top_text_narrow = get_sub_field('top_text_narrow_tx') ? 'large-8 large-offset-2' : null;
?>

<section class="text <?php echo $is_grey_bg_tx; ?>">

	<?php $top_text = get_sub_field('top_text'); ?>
	<?php if($top_text): ?>
	<div class="row component">
		<div class="small-12 <?php echo $top_text_narrow; ?> columns">
			<?php echo $top_text; ?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php get_template_part('components/image/image', null); ?>

	<?php if( have_rows('text_tx') ): ?>
			
		<?php while ( have_rows('text_tx') ) : the_row();

		$column_num = get_sub_field('column_number');

		switch($column_num) {
			case '1' :
				$text_layout = 'small-12 columns';
				break;
			case '2' : 
				$text_layout = 'small-12 large-6 columns';
				break;
			case '3' :
				$text_layout = 'small-12 large-4 columns';
				break;
			case '4' :
				$text_layout = 'small-12 large-3 columns';
				break;
			default:
				$text_layout = 'small-12 columns';
		}

		$text_one = get_sub_field('column_one');
		$text_two = get_sub_field('column_two');
		$text_three = get_sub_field('column_three');
		$text_four = get_sub_field('column_four');?>
	<div class="row">
		
		<?php if($text_one): ?>
		<div class="<?php echo $text_layout; ?>">
			<?php echo $text_one; ?>
		</div>
		<?php endif; ?>
		
		<?php if($text_two): ?>
		<div class="<?php echo $text_layout; ?>">
			<?php echo $text_two; ?>
		</div>
		<?php endif; ?>
		
		<?php if($text_three): ?>
		<div class="<?php echo $text_layout; ?>">
			<?php echo $text_three; ?>
		</div>
		<?php endif; ?>
		
		<?php if($text_four): ?>
		<div class="<?php echo $text_layout; ?>">
			<?php echo $text_four; ?>
		</div>
		<?php endif; ?>

	</div>
	<?php endwhile; endif; ?>

	<?php get_template_part('components/button/button', null); ?>
</section>
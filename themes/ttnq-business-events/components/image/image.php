<?php
// This component is used as a stand alone and in the blog template.
// Unfortunately the blog uses different fields hence the or's and ternaries
	$image_dir = get_sub_field('image_direction_im') || get_sub_field('image_direction') ? 'direction-right' : null;
	$image_content = get_sub_field('image_content_im') ? get_sub_field('image_content_im') : get_sub_field('image_content');
	$image_count = count($image_content);
	$i = 0;
 ?>

<?php if(have_rows('image_content_im') || have_rows('image_content')): ?>
<div class="row image-component-wrapper">
	<div class="image-gallery small-12 large-10 large-offset-1">

    <?php while ( have_rows('image_content_im') || have_rows('image_content')) : the_row();

    	$i++;
    	$caption_field = get_sub_field('caption');
    	$caption_margin = $caption_field ? null : 'mobile-margin';
		$caption = $caption_field && $image_count > 1 ? '<div class="image-caption"><p>'.$caption_field.'</p></div>' : null;
		$single_caption = $image_count == 1 && $caption_field ? '<div class="image-caption-below"><p>'.$caption_field.'</p></div>' : ($caption_field ? '<div class="image-caption-below-mobile"><p>'.$caption_field.'</p></div>' : null);

		switch($image_count) {
			case 1 :
				$layout = 'one-up-';
				break;
			case 2 : 
				$layout = 'two-up-';
				break;
			case 3 :
				$layout = 'three-up-';
				break;
			case 4 :
				$layout = 'four-up-';
				break;
			default:
				$layout = 'one-up-';
		}

		?>

		<a href="<?php echo wp_get_attachment_image_url(get_sub_field('image'), 'full'); ?>"
			class="lightbox-image image <?php echo $caption_margin; ?> <?php echo $image_dir ?> <?php echo $layout.$i; ?> image-<?php echo $i?>">

			<div class="image-thumb"
				 data-bp="<?php echo wp_get_attachment_image_url(get_sub_field('image'), 'full'); ?>"
				 style="background-image:url('<?php echo wp_get_attachment_image_url(get_sub_field('image'), 'large');?>')">
			</div>

			<?php echo $caption; ?>
		</a><?php echo $single_caption; ?>

	<?php endwhile; ?>

	</div>
</div>
<?php endif; ?>
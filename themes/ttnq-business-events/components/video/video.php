<?php

// Basic video component

$video = get_sub_field('video');

?>

<div class="video-tile">
	<?php echo $video; ?>
</div>
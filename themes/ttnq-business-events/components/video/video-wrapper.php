<?php $is_grey_bg_vd = get_sub_field('grey_background_vd') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_vd') == 'white' ? 'white-background' : null); ?>

<section class="video <?php echo $is_grey_bg_vd; ?>">
	<div class="row">
		<div class="small-12 large-6 columns">
			<?php get_template_part('components/video/video') ?>
		</div>
		<div class="small-12 large-6 columns">
			<?php the_sub_field('video_text'); ?>

			<?php get_template_part('components/button/button', null); ?>
		</div>
	</div>
</section>
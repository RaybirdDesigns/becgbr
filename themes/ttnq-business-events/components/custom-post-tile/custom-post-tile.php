<?php

// Custom post tile.
// Shows feature image and a snippet of the content.
// If it's an event it's show the date

// get date to split up
$date = strtotime(get_field('time_of_event'));
$date_end = strtotime(get_field('end_time_of_event'));

$price = get_field('price') ? '<span class="custom-post-tile-price">'.get_field('price').'</span>': null;

// get date type
$date_type = get_field('date_type');

// show the start date month if different from end date month
$start_month = date('M', $date) !== date('M', $date_end) ? date('M', $date) : null;

$tag_name = get_the_tags( $id ) ? (get_the_tags( $id )[0]->name == 'blank' ? get_the_tags( $id )[1]->name : get_the_tags( $id )[0]->name) : null;

$bg_image = has_post_thumbnail() ? responsive_image_post_return('large') : responsive_image_return( get_field('global_fallback_image', 'options'), 'large' );

?>

<a href="<?php the_permalink(); ?>"
   class="custom-post-tile"
   data-responsive-background-image >

   <img <?php echo $bg_image; ?> >

   <?php

   $post_type_name = get_post_type_object(get_post_type())->label;

   // If post type is event use the acf field event_category
   // otherwise use the first tag
   if ($post_type_name == 'event') {

		if (get_field('event_category')) {
			echo '<div class="custom-post-tile-tag">' . get_field('event_category') . ' ' . '</div>';
		}
	} elseif ($tag_name) {
		echo '<div class="custom-post-tile-tag">' . $tag_name . ' ' . '</div>';
	}

	if (is_user_logged_in()) {

		$priority_num = get_field('priority') ? get_field('priority') : 'Not set';
		echo '<div class="priority priority-'.get_field('priority').'">'.$priority_num.'</div>';
	}

	?>

	<div class="custom-post-tile-text">
		<?php if (get_field('time_of_event')) : ?>
			<?php if ($date_type == 'single-date') : ?>
				<span class="custom-post-tile-text-date"><?php the_field('time_of_event'); ?></span>
				<?php elseif ($date_type == 'date-range') : ?>
				<span class="custom-post-tile-text-date">
					<?php echo date('j', $date).' '.$start_month;?> - <?php echo date('j', $date_end).' '.date('M', $date_end).' '.date('Y', $date_end); ?>
				</span>
				<?php else : ?>
				<span class="custom-post-tile-text-date"><?php echo date('M', $date).' '.date('Y', $date);?></span>
			<?php endif; ?>
		<?php endif; ?>
		<h5><?php the_title(); ?><?php echo $price; ?></h5>
		<?php if($i == 1 && has_excerpt()) : ?>
		<?php the_excerpt(); ?>
		<?php endif; ?>
		<div class="custom-post-tile-text-view">View <?php echo $post_type_name; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></div>
		<div class="overlay"></div>
	</div>
</a>
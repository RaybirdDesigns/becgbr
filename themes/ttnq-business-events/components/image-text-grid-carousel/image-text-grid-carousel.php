<?php

// Component description:
// Allows multiple rows of image, button and text content
// Can appear as a single centered item, carousel or grid

$is_grey_bg_igtc = get_sub_field('grey_background_igtc') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_igtc') == 'white' ? 'white-background' : null);
?>

<section class="image-grid-text-carousel <?php echo $is_grey_bg_igtc; ?>">
	<div class="row component">
		<div class="small-12 large-8 large-offset-2 columns">
			<?php the_sub_field('grid_heading_igtc'); ?>
		</div>
	</div>
	<div class="row column">

		<?php $rows = get_sub_field('item_row_igtc'); ?>

		<?php if( have_rows('item_row_igtc') ): ?>

				<?php $num_rows = count($rows);?>

			    <?php while ( have_rows('item_row_igtc') ) : the_row(); ?>

					<?php
					$carousel = get_sub_field('is_carousel');
					$round_images = get_sub_field('round_images');
					$is_carousel = $carousel == true ? 'image-grid-text-carousel-carousel' : 'image-grid-text-carousel-wrapper';
					$is_multiple_rows = $num_rows > 1 ? 'image-grid-text-carousel-multi clearfix' : null;
					$is_round_images = $round_images == true ? 'round-images' : null;
					?>
					
					<?php if(get_sub_field('grid_sub_heading')): ?>
			        <h3><?php the_sub_field('grid_sub_heading'); ?></h3>
			    	<?php endif; ?>

					<div class="<?php echo $is_carousel; ?> <?php echo $is_multiple_rows; ?> <?php echo $is_round_images; ?>">

						<?php $items =  get_sub_field('items')?>

				        <?php if( have_rows('items') ): ?>

				        	<?php $num_items = count($items);?>

				        	<?php while ( have_rows('items') ) : the_row(); ?>

				        	<?php
				        		$is_single_item = $num_items == 1 ? 'image-grid-text-carousel-item-single' : null;
								$image_id = get_sub_field('image');
								$image_src = wp_get_attachment_image_url( $image_id, 'large' );
								$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);
								$grid_layout = !$is_single_item && $num_items > 1 && !$carousel ? 'small-12 medium-6 large-3 ' : null;
							?>
					       	<div class="image-grid-text-carousel-item <?php echo $is_single_item; ?> <?php echo $grid_layout; ?>">

					       		<?php if($image_id): ?>
					       		<div class="image-grid-text-carousel-item-img"
					       			title="<?php echo $image_alt; ?>"
					       			style="background-image: url(<?php echo $image_src; ?>)">
					       		</div>
					       		<?php endif; ?>

					       		<figure class="image-grid-text-carousel-item-text">

					       			<?php if(get_sub_field('button_url')): ?>
					       				<a class="btn btn-small btn-ghost" target="_blank" href="<?php addhttp(the_sub_field('button_url')); ?>"><?php the_sub_field('button_text'); ?> <i class="fa fa-external-link" aria-hidden="true"></i></a>
					       			<?php endif; ?>

					       			<?php if(get_sub_field('heading')): ?>
					       				<h4><?php the_sub_field('heading'); ?></h4>
					       			<?php endif; ?>

									<?php the_sub_field('body_copy'); ?>
					       		</figure>
					       	</div>
				        <?php endwhile; endif;?>
					</div>
		
		<?php endwhile; endif;?>
	</div>
</section>
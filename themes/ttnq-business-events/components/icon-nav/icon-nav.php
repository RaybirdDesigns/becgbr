<?php $is_grey_bg_in = get_sub_field('grey_background_in') == 'grey' ? 'grey-background' : (get_sub_field('grey_background_in') == 'white' ? 'white-background' : null); ?>


<style>
	
	.icon-nav-item-wrapper {
		border: 2px solid <?php echo $theme_colour; ?>;
	}

	.icon-nav-item-link:hover .icon-nav-item-wrapper,
	.icon-nav-item-link:focus .icon-nav-item-wrapper {
	    background-color: <?php echo $theme_colour; ?>;
	}

	.icon-nav-item-link:focus h6,
	.icon-nav-item-link:hover h6 {
		color: <?php echo $theme_colour; ?>;
	}

</style>

<section id="icon-nav" class="<?php echo $is_grey_bg_in; ?>">
	<div class="row looking-for-content">
		<div class="small-12 columns text-center">
			<h2><?php the_sub_field('nav_heading_in'); ?></h2>
		</div>
		<ul class="icon-nav-list list-reset clearfix">
		<?php
			if ( have_rows('icon_nav_items_in') ) {

				while ( have_rows('icon_nav_items_in') ) {

					the_row();

					$post_object = get_sub_field('icon_nav_item');

					if ( $post_object ) {

						$post = $post_object;
						setup_postdata( $post_object ); ?>

						<li class="small-6 medium-4 large-2">
							<?php include(locate_template('/components/icon-nav/icon-nav-item.php')); ?>
						</li>


		<?php			wp_reset_postdata();
					}
				}
			}
		?>
		</ul>
	</div>
</section>
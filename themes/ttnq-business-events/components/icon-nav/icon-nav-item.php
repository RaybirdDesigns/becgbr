<div class="icon-nav-item clearfix">
	<a class="icon-nav-item-link" href="<?php the_permalink(); ?>">
		<div class="icon-nav-item-wrapper">
			<?php if(get_sub_field('icon')): ?>
			<?php echo file_get_contents(htmlspecialchars(get_sub_field('icon')), false, null); ?>
			<?php endif; ?>
		</div>
	    <h6><?php the_title(); ?>&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></h6>
	</a>
</div>
<section class="image-text-grid-2-col collapse">
	<div class="full-width-image-text-grid-2-col clearfix" data-responsive-background-image >
		<img <?php responsive_image(get_sub_field('background_image_it2c'), 'full') ?> >
		<div class="row full-width-image-text-grid-2-col-wrapper">
			<?php if(get_sub_field('heading_it2c')): ?>
			<div class="clearfix component">
				<div class="small-12 columns text-center">
					<?php the_sub_field('heading_it2c'); ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="clearfix">
				<ul class="list-reset image-text-grid-2-col-list">
				<?php if(have_rows('content_it2c')): ?>
				<?php while ( have_rows('content_it2c') ) : the_row();?>
					<li class="small-12 large-6 columns">
						<div class="image-text-grid-2-col-image" data-responsive-background-image >
							<img <?php responsive_image(get_sub_field('image'), 'large') ?> >
						</div>
						<?php the_sub_field('text'); ?>
					</li>
				<?php endwhile; ?>
				<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php
/**
 * Template Name: Search results
 */
$theme_colour = get_field('global_colour_theme', 'options');

?>

<?php get_header(); ?>

<style>
	
	h1:before {
		border-bottom: 3px solid <?php echo $theme_colour ?>!important;
	}

	ul.pagination li.next a,
	ul.pagination li.previous a,
	button.btn {
		border: solid 3px <?php echo $theme_colour ?>!important;
	}

	ul.pagination li.next a:hover,
	ul.pagination li.previous a:hover,
	button.btn:hover {
		background-color: <?php echo $theme_colour ?>!important;
	}

</style>

<?php get_template_part('components/hero-banners/short-hero/short-hero', null) ?>

<div id="search-listing-root">
	<!-- This will be replaced by the react app -->
	<section id="search-header" class="search-header">
	   <div class="row">
	      <div class="small-12 columns text-center">
	         <h1>Search Results</h1>
	         <p>0 results found</p>
	         <div class="faceted-filter-wrapper">
	            <form class="small-12 large-8 large-offset-2">
	               <div class="stage-1 clearfix">
	                  <div class="css-10nd86i filter-margin small-12 large-4 columns">
	                     <div class="css-1aya2g8">
	                        <div class="css-1rtrksz">
	                           <div class="css-1492t68">Please Select a Category</div>
	                           <div class="css-rsyb7x">
	                              <div class="" style="display: inline-block;">
	                                 <input autocapitalize="none" autocomplete="off" autocorrect="off" id="react-select-2-input" spellcheck="false" tabindex="0" type="text" aria-autocomplete="list" value="" style="box-sizing: content-box; width: 2px; background: 0px center; border: 0px; font-size: inherit; opacity: 1; outline: 0px; padding: 0px; color: inherit;">
	                                 <div style="position: absolute; top: 0px; left: 0px; visibility: hidden; height: 0px; overflow: scroll; white-space: pre; font-size: 16px; font-family: sans-serif; font-weight: 400; font-style: normal; letter-spacing: normal; text-transform: none;"></div>
	                              </div>
	                           </div>
	                        </div>
	                        <div class="css-1wy0on6">
	                           <span class="css-d8oujb"></span>
	                           <div aria-hidden="true" class="css-1ep9fjw">
	                              <svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" class="css-19bqh2r">
	                                 <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
	                              </svg>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	                  <div class="css-10nd86i filter-margin small-12 large-4 columns">
	                     <div class="css-1aya2g8">
	                        <div class="css-1rtrksz">
	                           <div class="css-1492t68">Please Select a Region</div>
	                           <div class="css-rsyb7x">
	                              <div class="" style="display: inline-block;">
	                                 <input autocapitalize="none" autocomplete="off" autocorrect="off" id="react-select-3-input" spellcheck="false" tabindex="0" type="text" aria-autocomplete="list" value="" style="box-sizing: content-box; width: 2px; background: 0px center; border: 0px; font-size: inherit; opacity: 1; outline: 0px; padding: 0px; color: inherit;">
	                                 <div style="position: absolute; top: 0px; left: 0px; visibility: hidden; height: 0px; overflow: scroll; white-space: pre; font-size: 16px; font-family: sans-serif; font-weight: 400; font-style: normal; letter-spacing: normal; text-transform: none;"></div>
	                              </div>
	                           </div>
	                        </div>
	                        <div class="css-1wy0on6">
	                           <span class="css-d8oujb"></span>
	                           <div aria-hidden="true" class="css-1ep9fjw">
	                              <svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" class="css-19bqh2r">
	                                 <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
	                              </svg>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	                  <div class="text-search-input-container small-12 large-4 columns"><label class="show-for-sr" for="s">Company name</label><input class="text-search-input" id="s" name="s" type="search" placeholder="Company Name" value=""></div>
	               </div>
	               <button type="submit" class="btn btn-medium btn-ghost" disabled="">Search</button>
	            </form>
	         </div>
	      </div>
	   </div>
	</section>
</div>

<?php get_footer(); ?>
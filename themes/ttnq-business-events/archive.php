<?php
/**
 * Template Name: Post List
 */

$post_type_obj = get_post_type_object(get_post_type());


// check if we're listing events
$is_event_list = $post_type == 'event' ? true : false;

$args = array(
    'post_type'     => $post_type_obj->name,
    'post_status'   => 'publish',
    'no_found_rows' => false,
    'update_post_term_cache' => false,
    'posts_per_page' => 1,
    'paged'          => false,
);

$wp_query = new WP_Query($args);

$total_posts = $wp_query->found_posts;

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/short-hero/short-hero', null) ?>

<div id="list-header" class="list-header">
    <div class="row">
        <div class="small-12 columns text-center">
            <h1>All <?php echo $post_type_obj->label; ?></h1>
            <p><?php echo $total_posts; ?> results found</p>
        </div>
    </div>
</div>

<?php if($is_event_list): ?>
<div id="event-reorder" class="event-reorder">
    <div class="row">
        <div class="small-12 columns">
            <input class="order-by-date" id="orderByDate" type="checkbox"><label for="orderByDate">Order events by start date</label>
        </div>
    </div>
</div>
<?php endif; ?>

<section id="list" data-postType="<?php echo $post_type; ?>">
    <div class="row">
        <div id="list-container" class="small-12 columns">
            <!-- results ajaxed in here -->
        </div>
    </div>
</section>



<?php get_footer()?>
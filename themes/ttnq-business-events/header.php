<!doctype html>
<html class="no-js" lang="<?php echo get_locale() ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if(is_admin()): ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url')?>">
        <?php endif; ?>
        <link rel="icon" type="image/png" href="<?php the_field('favicon', 'option') ?>" sizes="64x64" />

        <?php wp_head() ?>

        <script type='text/javascript'>
        //<![CDATA[ 
        function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }loadCSS("https://use.fontawesome.com/1ff8671c70.css");
        //]]> 
        </script>
        
        <?php //the_field('header_scripts','option'); ?>
        
<?php
if($_SERVER['SERVER_NAME'] == 'businesseventscairns.org.au') {
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQM5NCS');</script>
<!-- End Google Tag Manager -->
<?php
}
?>

    </head>

    <body <?php body_class(); ?>>
        <?php //the_field('body_scripts','option'); ?>

<?php
if($_SERVER['SERVER_NAME'] == 'businesseventscairns.org.au') {
?>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQM5NCS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php
}
?>

        <!--[if lt IE 10]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="button-top-right" style="display: none;">
            <a href="/contact-us/subscribe/">
                Subscribe to Bureau Brief
            </a>
        </div>
        <script>
            jQuery(document).ready(function($) {
                setTimeout( function() {
                    $('.button-top-right').slideToggle();
                }, 30000);
            });
        </script>
        <style>.button-top-right {
                position: fixed;
                top: 0;
                right: 0;
                z-index: 999999;
            }

            .button-top-right a {
                background: #178b85;
                color: #fff;
                font-size: 18px;
                padding: 20px 10px;
                display: block;
                text-align: center;
            }</style>

        <div class="hide">
            <?php echo file_get_contents( htmlspecialchars(get_template_directory_uri() . '/images/sprites/global-sprite.svg'), false, null); ?>
        </div>
        
        <header class="header">
            <div class="row">
                <div class="header-wrapper">
                    <button class="main-menu-btn"><span class="hamburger"></span></button>
                    <a class="header-logo-link" href="<?php echo site_url(); ?>">
                        <svg class="header-logo" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tnq-logo"></use>
                        </svg>
                    </a>
                    <button type="submit" class="header-search-btn main-menu-item">
                        <svg class="header-search-ico" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
                        </svg>
                    </button>
                    <nav class="main-nav clearfix">
                        <div class="menu-header">
                            <h3 class="menu-header-title">Menu</h3>
                            <button class="menu-header-close-btn">
                                <svg class="menu-header-close-ico" role="presentation">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#times"></use>
                                </svg>
                            </button>
                        </div>
                        <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'walker' => new WPSE_78121_Sublevel_Walker ) ); ?>
                    </nav>
                </div>
            </div>
            <div class="header-search-wrapper">
                <div class="row columns">
                    <svg class="search-ico" role="presentation">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
                    </svg>
                    <div class="header-search-form"><?php get_search_form(); ?></div>
                    <button class="header-search-close-btn">
                        <svg class="search-close-ico" role="presentation">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#times"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </header>
        
        <div id="main">
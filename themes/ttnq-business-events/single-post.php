<?php
/*
Single Post Template: Article
Description: Template for an article
*/

$theme_colour = get_field('theme_colour') == 'global' ? get_field('global_colour_theme', 'options') : get_field('theme_colour');

?>

<?php get_header()?>

<style>

	.getting-there-icon {
		fill: <?php echo trim($theme_colour) ?>;
	}

	h2:before {
		border-bottom: solid 3px <?php echo $theme_colour ?>!important;
	}

	.flexible-button,
	.blog-button,
	button,
	a.btn {
		border: solid 3px <?php echo $theme_colour ?>!important;
	}

	.flexible-button:hover,
	.blog-button:hover,
	button:hover,
	a.btn:hover {
		background-color: <?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.flexible-button:focus,
	.blog-button:focus {
		background-color: <?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	a,
	a:hover,
	a:focus {
		border-bottom: solid 2px <?php echo $theme_colour ?>;
	}

	#excerpt .avatar-author,
	#excerpt .avatar-desc {
		color: <?php echo $theme_colour ?>;
	}

	section:nth-of-type(even) {
	    background-color: #ffffff;
	}
	
</style>

<div id="blog-sprite" class="hide">
    <?php echo file_get_contents( get_template_directory_uri() . '/images/sprites/blog-sprite.svg', false, null ); ?>
</div>

<?php get_template_part('components/hero-banners/hero-blog/hero', 'blog') ?>

<?php
if (have_posts()) :
	while (have_posts()) : the_post();
?>

<section id="article-content">
	
	<?php if(empty(have_rows('content_top'))) : ?>
	<section id="excerpt">
		<div class="row">
			<div class="small-12 medium-12 large-10 columns">
				<div class="avatar small-12 medium-2 float-left">
					<?php echo get_avatar( get_the_author_meta( 'ID' ), 150 ); ?>
					<div class="avatar-detail">
						<span itemprop="author" itemscope itemtype="http://schema.org/Person"><p itemprop="author" itemscope itemtype="http://schema.org/Person" class="avatar-author"><?php echo get_the_author_meta('display_name'); ?></p></span>
					</div>
				</div>
				<div class="excerpt small-12 medium-10 float-left">
					<?php the_excerpt(); ?>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php

	// check if the flexible content field has rows of data
	if( have_rows('content_top') ):

		echo '<section id="blog-getting-there">
				<div class="row">
					<div class="small-12 columns">';

	     // loop through the rows of data
	    while ( have_rows('content_top') ) : the_row();

	        if( get_row_layout() == 'getting_there' ):

	        	echo '<h2>'.get_sub_field('heading').'</h2>';

	        endif;

	        if( get_sub_field('getting_there_details') ):

	        echo '<ul class="getting-there-list list-reset list-inline clearfix">';

			 	// loop through the rows of data
			    while ( have_rows('getting_there_details') ) : the_row();

				$detail_select = get_sub_field('detail_select');
				$how_select = get_sub_field('detail_icon');

				$detail_label = $detail_select['label'];
				$detail_value = $detail_select['value'];
				$detail_icon = ($detail_value == 'how' && $how_select) ? $how_select : $detail_value;

				echo '<li>
						<svg class="getting-there-icon" role="presentation">
						<use xlink:href="'. get_template_directory_uri().'/images/sprites/blog-sprite.svg#'.$detail_icon.'"></use>
	                    </svg>
		                <p class="getting-there-label">'.$detail_label.'</p>
		                <p class="getting-there-detail">'.get_sub_field('detail_text').'</p>
		              </li>';

				endwhile;

			echo '</ul>';

			endif;

	    endwhile;

	    echo '</div></div></section>';

	endif;

	?>

		
	<?php

		// check if the flexible content field has rows of data
	if( have_rows('flexible_content') ):

		$section_index = 0;

		     // loop through the rows of data
		while ( have_rows('flexible_content') ) : the_row();

			$section_index++;
		    	// +++++++++++++++++++ Start: Text Block +++++++++++++++++++++++

		    if( get_row_layout() == 'text_block' ):

		        $grey_background = get_sub_field('grey_background') ? 'grey-background' : 'white-background';

		        echo '<div class="flexible-text-wrapper flexible-text-wrapper-'.$section_index.' '.$grey_background.'">
		        		<div class="row">
		        			<div class="text-block clearfix small-12 medium-12 large-8 large-offset-2">';

		        	if( get_sub_field('heading') ):

		        		echo '<h2>'.get_sub_field('heading').'</h2>';

		        	endif;

		        	// ++++++++++++++++++++++++

		        	if(get_sub_field('text') && get_sub_field('columns') == 1):
		        	
						echo '<div class="small-12 columns">';
		        			the_sub_field('text');
		        		echo '</div>';


		        	elseif(get_sub_field('text') && get_sub_field('columns') == 2):

		        		echo '<div class="clearfix"><div class="small-12 medium-12 large-6 columns">';

		        			the_sub_field('text');

		        		echo '</div>';

		        	endif;

		        	// +++++++++++++++++++++++++

		        	if(get_sub_field('text_2') && get_sub_field('columns') == 2):

		        		echo '<div class="small-12 medium-12 large-6 columns">';

		        			the_sub_field('text_2');

		        		echo '</div></div>';

		        	endif;

		        	// +++++++++++++++++++++++++

		        echo '</div></div></div>';

		        endif;

			        // +++++++++++++++++++ Start: Text and Image +++++++++++++++++++++++

			        if( get_row_layout() == 'text_and_image' ):

			        	$grey_background = get_sub_field('grey_background') ? 'grey-background' : 'white-background';

			        	echo '<div class="flexible-text-and-image-wrapper '.$grey_background.'">';

			        		if( get_sub_field('heading') ):

				        		echo '<div class="row component">
				        				<h2 class="text-center small-12 medium-12 large-6 large-offset-3">'.get_sub_field('heading').'</h2></div>';

				        	endif;

				        	// +++++++++++++++++++++++++

				        	// check if the nested repeater field has rows of data
				        	if( get_sub_field('image_content') ):
				        		
				        		get_template_part('components/image/image', null);

							endif;

							if( get_sub_field('video') ):

								$has_caption = get_sub_field('video_caption') ? 'has-caption' : null;

								echo '<div class="row"><div class="small-12 large-10 large-offset-1"><div class="flexible-video-wrapper '.$has_caption.'">';
								the_sub_field('video');
								echo '</div></div></div>';

								if (get_sub_field('video_caption')) :
									echo '<div class="flexible-image-caption-below video-caption"><p>'.get_sub_field('video_caption').'</p></div>';
								endif;

							endif;

							// +++++++++++++++++++++++++++++++++++++++++++++++++

							if( get_sub_field('text') ):

				        		echo '<div class="row">
				        				<div class="clearfix"><div class="small-12 medium-12 large-8 large-offset-2 columns">';

			        			the_sub_field('text');

			        			echo '</div></div>';

				        	endif;

				        	// +++++++++++++++++++++++++++++++++++++++++++++++++

				        	if( get_sub_field('button_url') || get_sub_field('button_link') ):

				        		$button_url = get_sub_field('button_url') ? get_sub_field('button_url') : get_sub_field('button_link');
				        		$icon = get_sub_field('button_url') ? 'window-restore' : 'chevron-right';
				        		$target = get_sub_field('button_url') ? 'target="_blank"' : null;

				        		echo '<div class="text-center"><a '.$target.' class="flexible-button btn btn-medium btn-ghost" href="'.$button_url.'">'.get_sub_field('button_text').' <i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></div>';

				        	endif;

			        	echo '</div></div>';

			        endif;

			    	// +++++++++++++++++++ End: Text and image ++++++++++++++++++++++++++++++++++++

			    endwhile;

			endif;

		endwhile;

	endif;
	?>
</section>

<?php get_template_part('components/articles/articles'); ?>

<?php if(get_field('related_full_width_bg')) : 

// internal or external link. External overrides internal.
$full_width_banner_url = get_field('related_pages_full_width_external_url') ? addhttp(get_field('related_pages_full_width_external_url')) : get_field('related_pages_full_width_url');
$target = get_field('related_pages_full_width_external_url') ? 'target="_blank"' : null;

// add image below CTA button. Can set max width.
$image_caption = get_field('banner_image_caption');
$image_caption_width = get_field('banner_image_caption_width') ? get_field('banner_image_caption_width') : '300';

$tracking_id = get_field('banner_tracking_id') ? 'data-ttnq-id="' . get_field('banner_tracking_id') . '"' : null;

?>
<section id="full-width-related-blog" class="collapse">
	<div <?php echo $tracking_id; ?> class="full-width-related clearfix text-center" data-responsive-background-image >
		<img <?php responsive_image(get_field('related_full_width_bg'), 'full') ?> >
		<div class="full-width-related-content">
			<?php the_field('related_pages_full_width_text'); ?>
			<a <?php echo $tracking_id; ?> class="blog-button btn btn-medium btn-ghost btn-margin" <?php echo $target; ?> href="<?php echo $full_width_banner_url; ?>"><?php the_field('related_pages_button_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			<?php if ($image_caption): ?>
			<img style="margin-top: 20px; width:100%; transform: translate3d(0,0,0); max-width: <?php echo $image_caption_width; ?>px;" src="<?php echo $image_caption['url'] ?>" alt="<?php echo $image_caption['alt'] ?>">
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_footer()?>
<?php
/**
 * Template Name: Post List
 */

$category = get_the_category();
$first_category = isset($category[0]) ? $category[0]->slug : null;

$tags = get_the_tags();
$first_tag = isset($tags[0]) ? $tags[0]->name : null;

$post_type = get_field('post_type_lst') ? get_field('post_type_lst') : '';

// check if we're listing events
$is_event_list = $post_type == 'event' ? true : false;

$args = array(
    'post_type'     => $post_type,
    'category_name' => $first_category,
    'tag'           => $first_tag,
    'post_status'   => 'publish',
    'no_found_rows' => false,
    'update_post_term_cache' => false,
    'posts_per_page' => 1,
    'paged'          => false,
);

$wp_query = new WP_Query($args);

$total_posts = $wp_query->found_posts;

?>

<?php get_header()?>

<?php get_template_part('components/hero-banners/short-hero/short-hero', null) ?>

<div id="list-header" class="list-header">
    <div class="row">
        <div class="small-12 columns text-center">
            <h1><?php the_field('heading_lst'); ?></h1>
            <p><?php echo $total_posts; ?> results found</p>
        </div>
    </div>
</div>

<?php if($is_event_list): ?>
<div id="event-reorder" class="event-reorder">
    <div class="row">
        <div class="small-12 columns">
            <input class="order-by-date" id="orderByDate" type="checkbox"><label for="orderByDate">Order events by start date</label>
        </div>
    </div>
</div>
<?php endif; ?>

<section id="list"
         data-category="<?php echo $first_category; ?>"
         data-postType="<?php echo $post_type; ?>"
         data-tag="<?php echo $first_tag; ?>">
    <div class="row">
        <div id="list-container" class="small-12 columns">
            <!-- results ajaxed in here -->
        </div>
    </div>
</section>



<?php get_footer()?>
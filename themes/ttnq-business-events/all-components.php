<?php
/**
 * Template Name: All Components
 */

$theme_colour = get_field('theme_colour') == 'global' ? get_field('global_colour_theme', 'options') : get_field('theme_colour');

?>

<?php get_header()?>

<style>
	
	.btn,
	a.btn {
		border: solid 3px <?php echo $theme_colour ?>;
	}

	.btn:hover {
		background-color: <?php echo $theme_colour ?>;
		border: solid 3px <?php echo $theme_colour ?>;
	}

	a,
	a:hover {
		color: #<?php echo $theme_colour ?>;
		border-bottom: solid 2px <?php echo $theme_colour ?>;
	}

	h1:before,
	h2:before {
		border-bottom: 3px solid <?php echo $theme_colour ?>!important;
	}
	
	.icon-text-icon-wrapper svg path {
		fill: <?php echo $theme_colour ?>;
	}

</style>

<?php if( have_rows('all_components') ): ?>

    <?php while ( have_rows('all_components') ) : the_row(); ?>

    	<?php if( get_row_layout() == 'standard_hero' ): ?>

        	<?php get_template_part('components/hero-banners/standard-hero/standard-hero', null) ?>

        <?php elseif( get_row_layout() == 'short_hero' ): ?>
	
			<?php get_template_part('components/hero-banners/short-hero/short-hero', null) ?>

        <?php elseif( get_row_layout() == 'icon_nav' ): ?>

        	<?php include(locate_template('/components/icon-nav/icon-nav.php')); ?>

        <?php elseif( get_row_layout() == 'banner_ad' ): ?>
			
			<?php include(locate_template('/components/banner-ad/banner-ad-wrapper.php')); ?>

        <?php elseif( get_row_layout() == 'article_carousel' ): ?>

			<?php get_template_part('components/articles/articles', null); ?>

        <?php elseif( get_row_layout() == 'events' ): ?>

			<?php include(locate_template('/components/events/events-wrapper.php')); ?>

        <?php elseif( get_row_layout() == 'image_button_text_grid_or_carousel' ): ?>

        	<?php include(locate_template('/components/image-text-grid-carousel/image-text-grid-carousel.php')); ?>

        <?php elseif( get_row_layout() == 'stackla' ): ?>

        	<?php include(locate_template('/components/stackla/stackla.php')); ?>

    	<?php elseif( get_row_layout() == 'text_image' ): ?>

        	<?php include(locate_template('/components/text/text.php')); ?>

        <?php elseif( get_row_layout() == 'video_text' ): ?>

        	<?php include(locate_template('/components/video/video-wrapper.php')); ?>

        <?php elseif( get_row_layout() == 'icon_text_grid' ): ?>

        	<?php include(locate_template('/components/icon-text-grid/icon-text-wrapper.php')); ?>

        <?php elseif( get_row_layout() == 'image_text_2_column' ): ?>

        	<?php include(locate_template('/components/image-text-grid-2-col/image-text-grid-2-col.php')); ?>

        <?php elseif( get_row_layout() == 'product_tiles' ): ?>

        	<?php include(locate_template('/components/product-tiles/product-tiles-wrapper.php')); ?>

    	<?php elseif( get_row_layout() == 'quick_search' ): ?>
			<section>
        		<?php include(locate_template('/components/quick-search/quick-search.php')); ?>
			</section>

    	<?php elseif( get_row_layout() == 'full_width_banner' ): ?>

        	<?php include(locate_template('/components/full-width-banner/full-width-banner.php')); ?>

    	<?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>


<?php get_footer()?>

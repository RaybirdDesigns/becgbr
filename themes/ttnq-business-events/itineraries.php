<?php
/**
 * Template Name: Itineraries
 */

$theme_colour = get_field('theme_colour') == 'global' ? get_field('global_colour_theme', 'options') : get_field('theme_colour');
$sights_field = get_field('sights_categories');
$itineraryType = get_field('itinerary_type');
$itineraryLengthText = get_field('filter_length_text');
$itinerarySightsText = get_field('filter_sights_text');
$itineraryTypeValue = $itineraryType['value'];
$itineraryTypeKey = $itineraryType['label'];

?>


<?php get_header()?>

<script>
	//<![CDATA[ 
	var sightsArr = <?php echo json_encode($sights_field); ?>
	//]]>
</script>

<style>

	h2:before {
		border-bottom: solid 3px <?php echo $theme_colour ?>!important;
	}

	.btn {
		border: solid 3px <?php echo $theme_colour ?>!important;
	}

	.btn:hover {
		background-color: <?php echo $theme_colour ?>!important;
		color: #ffffff!important;
	}

	.itinerary-icon svg {
		fill: <?php echo $theme_colour ?>!important;
	}
</style>

<?php get_template_part('components/hero-banners/hero-itinerary/hero-itinerary'); ?>

<?php if (have_posts()) : ?>
<section id="overview" class="itinerary-overview">
	<div class="row">
		<div class="overview-body small-12 medium-10 medium-offset-1 large-8 large-offset-2 columns text-center">
			<?php
			while (have_posts()) : the_post();
				the_content();
			endwhile;
			?>
		</div>
	</div>
</section>
<?php endif; ?>

<section id="itineraries-root"
		 data-itinerarytype="<?php echo $itineraryTypeValue; ?>"
		 data-initiallyshow="<?php the_field('initially_show'); ?>"
		 data-itineraryname="<?php echo $itineraryTypeKey; ?>"
		 data-lengthtext="<?php echo $itineraryLengthText; ?>"
		 data-sightstext="<?php echo $itinerarySightsText; ?>"></section>

<?php if (get_field('stackla_html_snippet')) : ?>
<?php include(locate_template('/components/stackla/stackla.php')); ?>
<?php endif; ?>


<?php get_footer()?>
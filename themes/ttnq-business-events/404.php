<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
?>

<?php get_header(); ?>

<?php
// get thumbnamil
$thumb_nail_url = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'hero-thumb')[0];
$full_res_desktop = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'full')[0];
$full_res_mobile = wp_get_attachment_image_src(get_field('global_fallback_image', 'option'), 'mobile-banner')[0];

?>

<style>
    
    .short-hero:before {
        content: '';
        display: block;
        width: 100%;
        height:100%;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url('<?php echo base_64_convert($thumb_nail_url); ?>');
        filter: blur(10px);
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }

    .short-hero {
        background-image: url(<?php echo $full_res_desktop; ?>);
        height: 139px;
    }

    @media (max-width: 640px) {
        .short-hero {
            background-image: url(<?php echo $full_res_mobile; ?>);
        }
    }

</style>



<div id="hero-banner" class="short-hero hero-banner">
    <img class="hero-img" src="<?php echo $full_res_desktop; ?>" >
</div>

<section id="missing" class="missing">
    <div class="row">
        <div class="small-12 columns text-center">
            <h1>404</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="search-form-wrapper">
            <h2 class="text-center">Try searching?</h2>
                <svg class="search-ico" role="presentation">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
                </svg>
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                    <label>
                        <span class="show-for-sr">Search for:</span>
                        <input type="search" class="search-field" placeholder="Search Business Events Cairns & Great Barrier Reef" value="" name="s" title="Search for:" />
                    </label>
                </form>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
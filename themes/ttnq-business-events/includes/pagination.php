<?php

function tnq_get_paginate_links() {
    ob_start();
    ?>
        <div class="pages clearfix">
            <?php
                global $wp_query;
                $current = max( 1, $paged );
                $pagination = paginate_links( array(
                    'base' => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
                    'format' => '?paged=%#%',
                    'current' => $current,
                    'total' => $list_loop->max_num_pages,
                    'type' => 'array',
                    'prev_text' => '',
                    'next_text' => '',
                ) ); ?>
            <?php if ( ! empty( $pagination ) ) : ?>
                <ul class="pagination">
                    <?php foreach ( $pagination as $key => $page_link ) : ?>
                        <li class="paginated_link<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>"><?php echo $page_link ?></li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>
    <?php
    $links = ob_get_clean();
    return apply_filters( 'tnq_paginate_links', $links );
}
function tnq_paginate_links() {
    echo tnq_get_paginate_links();
}
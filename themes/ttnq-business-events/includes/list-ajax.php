<?php

// hook or 'action'. call 'ajax_post_list' on ajax request
add_action('wp_ajax_post_list', 'ajax_post_list');
add_action('wp_ajax_nopriv_post_list', 'ajax_post_list');

// functioncalled on every ajax request with action name 'post_list'
function ajax_post_list() {

	// all query params passed through the with ajax url
	$query_data = $_GET;
	// print_r($query_data);
	
	// get post type from ajax url query string
	$post_type = $query_data['postType'] ? $query_data['postType'] : '';
	// get category from ajax url query string
	$category = $query_data['category'] ? $query_data['category'] : '';
	// get tag from ajax url query string
	$tag = $query_data['tag'] ? $query_data['tag'] : '';
	// get meta_key from ajax url query string
	$meta_key = $query_data['metaKey'] ? $query_data['metaKey'] : '';

	$paged = (isset($query_data['paged']) ) ? intval($query_data['paged']) : 1;

	// set sort_type
	if ('event' && $meta_key == 'time_of_event') {
		$sort_type = 'event_date_sort';
	} elseif ($post_type == 'event' && $meta_key == '') {
		$sort_type = 'event_priority_sort';
	} elseif ($post_type == 'post') {
		$sort_type = 'post';
	} else {
		$sort_type = 'default';
	}

	// Determine which sorting method to use based on $sort_type. Options are:
	// > 'event_priority_sort' = event list sorted by priority and start date
	// > 'event_date_sort' = event list sorted just by start date (toggle in ui)
	// > 'post' = posts are ordered from newest to oldest
	// > default = normal post list sorted by priority
	switch ($sort_type) {
	    case 'event_priority_sort':
	        $list_loop = priority_event_sort(7, $paged, $category, $tag);
	        break;
	    case 'event_date_sort':
	        $list_loop = event_sort(7, $paged, $category, $tag, $meta_key);
	        break;
	    case 'post':
	        $list_loop = post_sort(7, $paged, $category, $tag, $meta_key);
	        break;
	    default:
	        $list_loop = priority_sort(7, $paged, $post_type, $category, $tag);
	}
	
	if( $list_loop->have_posts() ):

		echo '<ul class="list-reset">';

		while( $list_loop->have_posts() ): $list_loop->the_post();

			$post_image = has_post_thumbnail( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' )[0] : wp_get_attachment_image_src( get_field('global_fallback_image', 'options'), 'medium' )[0];
			$event_time = get_field('time_of_event') ? get_field('time_of_event') : null;

            echo '<li class="post-result" id="post-'.get_the_ID().'">';
            echo 	'<a class="post-result-item" href="'.get_the_permalink().'" title="Permanent Link to '.get_the_title().'">';
            echo     '<div class="post-result-image" style="background-image: url('.$post_image.');"></div>';
            echo     '<div class="post-result-text">';

            if ($event_time) {
            	echo 	 '<span class="post-result-text-date">'.$event_time.'</span>';
            }
            
	        echo 	    '<h4>'.get_the_title().'</h4>';

			            if ( function_exists('the_excerpt') ) {
			                echo '<p>'.get_the_excerpt().'</p>';
			            }

			            if (is_user_logged_in()) {
			            	// show priority notification if admin logged in
			            	$priority_num = get_field('priority') ? get_field('priority') : 'Not set';

			            	echo '<div class="tnq-notification priority priority-'.get_field('priority').'">Priority: '.$priority_num.'</div>';
			            }
            echo    '</div>'; 
            echo  '</a>';
            echo '</li>';

		endwhile; ?>

		</ul>

		<div class="pagination-wrapper">

		<div class="pages clearfix">
            <?php
                global $wp_query;
                $current = max( 1, $paged );
                $pagination = paginate_links( array(
                    'base' => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
                    'format' => '?paged=%#%',
                    'current' => $current,
                    'total' => $list_loop->max_num_pages,
                    'type' => 'array',
                    'prev_text' => '',
                    'next_text' => '',
                ) ); ?>

            <?php if ( ! empty( $pagination ) ) : ?>
                <ul class="pagination">
                    <?php foreach ( $pagination as $key => $page_link ) : ?>

                    	<?php
                    	$current_page = strpos( $page_link, 'current' ) !== false ? 'active' : '';
                    	$next = strpos( $page_link, 'next' ) !== false ? 'next' : '';
                    	$prev = strpos( $page_link, 'prev' ) !== false ? 'previous' : '';
                    	?>

                        <li class="<?php echo $current_page ?> <?php echo $next ?> <?php echo $prev ?>">
                        	<?php echo $page_link ?>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>
		</div>	
	<?php else: ?>

		<section id="no-search-results" class="component">
		    <div class="row">
		        <div class="small-12 columns">
		            <div class="search-form-wrapper text-center">
		                <p><strong>Sorry we couldn't find and results.</strong></p>
		            </div>
		        </div>
		    </div>
		</section>
<?php
	endif;
	wp_reset_postdata();
	
	die();
} ?>
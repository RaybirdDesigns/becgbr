<?php

/**
 * Hook into the REST API and include acf hero image in api response 
 */
function slug_register_featured_img() {
    register_rest_field( 'post',
        'hero_image_sb', // Key added to the response
        array(
            'get_callback'    => 'get_acf_standard_hero_image', // Callback function - returns the value
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
add_action( 'rest_api_init', 'slug_register_featured_img' );

function get_acf_standard_hero_image( $object, $field_name, $request ) {
	// Check if ACF plugin activated
	if ( function_exists( 'get_field' ) ) {
		// Get the value
		if ( $image = get_field('all_components', $object['id'] ) ) :

			$all_components = $object['acf']['all_components'][0];

			if (isset($all_components['hero_image_sb'])) {
				$image_id = $all_components['hero_image_sb'];
				return wp_get_attachment_image_url($image_id, 'medium');

			} else if (isset($all_components['background_image_sh'])) {
				$image_id = $all_components['background_image_sh'];
				return wp_get_attachment_image_url($image_id, 'medium');
			}

		endif;
	} else {
		return '';
	}
}




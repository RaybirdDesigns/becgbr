<?php
// Custom endpoints used for various search queries.

// ===============================================================================================
// This is used for string based queries in the quick search module
// ===============================================================================================
add_action( 'rest_api_init', 'custom_api_string_search' );   

function custom_api_string_search() {
    register_rest_route( 'custom/v1', '/str', array(
        'methods' => 'GET',
        'callback' => 'custom_api_string_search_callback'
    ));
}
// Used in the quick search.
function custom_api_string_search_callback( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    // Receive and set the page parameter from the $request for pagination purposes
    $s = $request->get_param( 's' ) ? $request->get_param( 's' ) : null;
    $paged = $request->get_param( 'page' );

    $posts_per_page = $request->get_param( 'posts_per_page' );
    $cat_ID = $request->get_param( 'categoryID' ) ?$request->get_param( 'categoryID' ) : null;
    $post_type = $request->get_param( 'post_type' ) == 'undefined' || $request->get_param( 'post_type' ) == null ? array('accommodation', 'offsite_venues', 'dining_restaurants', 'conference_venue', 'activities', 'professional_support', 'post', 'page') : $request->get_param( 'post_type' );
    $paged = ( isset( $paged ) || ! ( empty( $paged ) ) ) ? $paged : 1;

    $quick_search_swp_query = new WP_Query( array(
            's' => $s,
            'paged' => $paged,
            'cat' => $cat_ID,
            'post_type' => $post_type,
            'post_status' => 'publish',
            'update_post_term_cache' => false,
            'posts_per_page' => $posts_per_page,            
            // // Get all posts, that have priority set.
            'meta_query' => array(
                array(
                    'key'     => 'priority',
                    'compare' => 'EXISTS',
                ),
            ),
            // // Order first by priority (1, 2, 3, unset) and then Alpha within
            // // priority.
            'orderby' => array(
                'meta_value_num' => 'ASC',
                'title'           => 'ASC',
            ),
        )
    ); 
    $posts = $quick_search_swp_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $priority = get_field('priority', $id);
        $post_thumbnail = has_post_thumbnail( $id ) ? wp_get_attachment_image_url(get_post_thumbnail_id( $id ), 'medium') : null;
        $post_link = get_permalink( $id, false );
        $post_excerpt = wp_trim_words($post->post_content, 20);
        $category_name = get_the_category($id)[0]->name;

        $posts_data[] = (object) array(
            'id' => $id,
            'priority' => $priority,
            'title' => $post->post_title,
            'category' => $category_name,
            'type' => $post->post_type,
            'title' => array('rendered' => $post->post_title),
            'link' => $post_link,
            'better_featured_image' => array('media_details' => array('sizes' => array('medium' => array('source_url' => $post_thumbnail)))),
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>'),
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $quick_search_swp_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $quick_search_swp_query->max_num_pages );

    return $response;                   
}

// ===============================================================================================
// This is used for non string based queries in the quick search module
// ===============================================================================================
add_action( 'rest_api_init', 'custom_api_get_all_posts' );   

function custom_api_get_all_posts() {
    register_rest_route( 'custom/v1', '/all-posts', array(
        'methods' => 'GET',
        'callback' => 'custom_api_get_all_posts_callback'
    ));
}
// Used in the quick search.
function custom_api_get_all_posts_callback( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    // Receive and set the page parameter from the $request for pagination purposes
    $paged = $request->get_param( 'page' );
    $posts_per_page = $request->get_param( 'posts_per_page' );
    $category = $request->get_param( 'category' );
    $tag = $request->get_param( 'tag' );
    $post_type = $request->get_param( 'engine' ) ? $request->get_param( 'engine' ) : array( 'accommodation', 'offsite_venues', 'dining_restaurants', 'conference_venue', 'activities', 'professional_support');
    $paged = ( isset( $paged ) || ! ( empty( $paged ) ) ) ? $paged : 1;

    $quick_search_wp_query = new WP_Query( array(
            'paged' => $paged,
            'cat' => $category,
            'tag' => $tag,
            'post_status' => 'publish',
            'update_post_term_cache' => false,
            'posts_per_page' => $posts_per_page,            
            'post_type' => $post_type,
            // Get all posts, that have priority set.
            'meta_query' => array(
                array(
                    'key'     => 'priority',
                    'compare' => 'EXISTS',
                ),
            ),
            // Order first by priority (1, 2, 3, unset) and then randomise within
            // priority.
            'orderby' => array(
                'meta_value_num' => 'ASC',
                'rand'           => 'DESC',
            ),
        )
    ); 
    $posts = $quick_search_wp_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $priority = get_field('priority', $id);
        $post_thumbnail = has_post_thumbnail( $id ) ? wp_get_attachment_image_url(get_post_thumbnail_id( $id ), 'medium') : null;
        $post_link = get_permalink( $id, false );
        $post_excerpt = wp_trim_words($post->post_content, 20);
        $category_name = get_the_category($id)[0]->name;

        $posts_data[] = (object) array(
            'id' => $id,
            'priority' => $priority,
            'title' => $post->post_title,
            'category' => $category_name,
            'type' => $post->post_type,
            'title' => array('rendered' => $post->post_title),
            'link' => $post_link,
            'better_featured_image' => array('media_details' => array('sizes' => array('medium' => array('source_url' => $post_thumbnail)))),
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>'),
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $quick_search_wp_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $quick_search_wp_query->max_num_pages );

    return $response;                   
}

// ===============================================================================================
// This is used to get the custom taxonomy terms for the faceted search
// ===============================================================================================
add_action( 'rest_api_init', 'custom_api_get_all_custom_tax' );

function custom_api_get_all_custom_tax() {
    register_rest_route( 'custom/v1', '/all-custom-tax', array(
        'methods' => 'GET',
        'callback' => 'custom_api_get_all_cust_tax_callback'
    ));
}

function custom_api_get_all_cust_tax_callback( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    // Receive and set the page parameter from the $request for pagination purposes
    $post_type = $request->get_param( 'engine' );
    
    // Get the posts using the 'post' and 'news' post types
    $cust_tax_wp_query = new WP_Query( array(
            'post_type' => $post_type,
            'paged' => false,
            'post_status' => 'publish',
            'update_post_term_cache' => false,
            'posts_per_page' => -1       
        )
    ); 
    $posts = $cust_tax_wp_query->posts;

    $custom_tax_types = array(
        "accommodation"=>"accommodation_type",
        "activities"=>"activity_type",
        "professional_support"=>"support_type",
        "conference_venue"=>"conference_type",
        "dining_restaurants"=>"dining_restaurant_types",
        "offsite_venues"=>"off_site_venue_types"
    );

    // Get first post and push the desired data to the array we've initialized earlier in the form of an object
    $id = $posts[0]->ID;
    $post_type = $posts[0]->post_type;
    $tax_type = $custom_tax_types[$post_type];
    $cust_tax = get_terms( array(
        'taxonomy' => $custom_tax_types[$post_type],
        'hide_empty' => false,
    ) );

    $posts_data[] = (object) array(
        'id' => $id,
        'cust_tax' => $cust_tax,
    );

    return $posts_data;                   
}


// ===============================================================================================
// This is used to get the facetted search
// ===============================================================================================

/*
example query string
/wp-json/custom/v1/facet-query?s=accom&postType=accommodation&taxType=accommodation_type&categoryID=14&capacity=100,120&tags=hotel,hostel&size=10,1000&distance=90,110&rooms=2,6
*/

add_action( 'rest_api_init', 'custom_api_facet_query' );

function custom_api_facet_query() {
    register_rest_route( 'custom/v1', '/facet-query', array(
        'methods' => 'GET',
        'callback' => 'custom_api_get_facet_query'
    ));
}

function custom_api_get_facet_query( $request ) {
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    $custom_tax_types = array(
        "accommodation"=>"accommodation_type",
        "activities"=>"activity_type",
        "professional_support"=>"support_type",
        "conference_venue"=>"conference_type",
        "dining_restaurants"=>"dining_restaurant_types",
        "offsite_venues"=>"off_site_venue_types"
    );

    // Receive and set the page parameter from the $request for pagination purposes
    $searchTerm = $request->get_param( 's' );
    $post_type = $request->get_param( 'postType' );
    $cat_ID = $request->get_param( 'categoryID' );
    $taxonomy = $request->get_param( 'taxType' );
    $tags = $request->get_param( 'tags' );
    $size = $request->get_param( 'size' );
    $capacity = $request->get_param( 'capacity' );
    $distance = $request->get_param( 'distance' );
    $rooms = $request->get_param( 'rooms' );
    $paged = $request->get_param( 'page' );
    $paged = ( isset( $paged ) || ! ( empty( $paged ) ) ) ? $paged : 1;
    $posts_per_page = $request->get_param( 'posts_per_page' );

    // convert comma separated tag list to array
    $tag_arr = explode(",",$tags);

    $args = array (
        's' => $searchTerm,
        'post_type'=> $post_type,
        'cat' => $cat_ID,
        'paged' => $paged,
        'post_status' => 'publish',
        'update_post_term_cache' => false,
        'posts_per_page' => $posts_per_page,
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => $taxonomy,
                'terms' => $tag_arr,
                'field' => 'slug',
                'operator' => "IN",
            )
        ),
        'meta_query' => array (
            array (
                'key'     => 'capacity',
                'value'   => $capacity,
                'type'    => 'numeric',
                'compare' => 'BETWEEN',
            ),
            array (
                'key'     => 'distance_to_venue',
                'value'   => $distance,
                'type'    => 'numeric',
                'compare' => 'BETWEEN',
            ),
            array (
                'key'     => 'area_sq_m',
                'value'   => $size,
                'type'    => 'numeric',
                'compare' => 'BETWEEN',
            ),
            array (
                'key'     => 'number_of_rooms',
                'value'   => $rooms,
                'type'    => 'numeric',
                'compare' => 'BETWEEN',
            ),
            'priority_clause' => array(
                'key'     => 'priority',
                'compare' => 'EXISTS',
            ),
        ),
        'orderby' => array(
            'priority_clause' => 'ASC',
            'title'           => 'ASC',
        ),
    );
    
    // Get the posts using the 'post' and 'news' post types
    $facet_wp_query = new WP_Query($args); 

    $posts = $facet_wp_query->posts;

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach( $posts as $post ) {
        $id = $post->ID;
        $priority = get_field('priority', $id);
        $post_thumbnail = has_post_thumbnail( $id ) ? wp_get_attachment_image_url(get_post_thumbnail_id( $id ), 'medium') : null;
        $post_link = get_permalink( $id, false );
        $post_excerpt = wp_trim_words($post->post_content, 20);
        $category_name = get_the_category($id)[0]->name;

        $posts_data[] = (object) array(
            'id' => $id,
            'priority' => $priority,
            'title' => $post->post_title,
            'category' => $category_name,
            'type' => $post->post_type,
            'title' => array('rendered' => $post->post_title),
            'link' => $post_link,
            'better_featured_image' => array('media_details' => array('sizes' => array('medium' => array('source_url' => $post_thumbnail)))),
            'excerpt' => array('rendered' => '<p>'.$post_excerpt.'</p>'),
        );
    }

    $response = new WP_REST_Response( $posts_data );

    $response->header( 'X-WP-Total', (int) $facet_wp_query->found_posts );
    $response->header( 'X-WP-TotalPages', (int) $facet_wp_query->max_num_pages );

    return $response;                   
}


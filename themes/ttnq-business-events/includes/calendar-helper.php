<?php

// Get selected categories from acf field
// and return associative array of category id => name
// Used in calendar and search apps
function calendar_filters() {
	$filters = get_field('categories_to_filter');
	$categories = get_categories();

	// create array of category id => name
	foreach($categories as $category) {
		$catname = $category->name;
		$catid = (string)$category->term_id;
		$categoryArr[$catid] = $catname;
	}

	// filter by the selected categories
	$filtered = array_filter(
	    $categoryArr,
	    function ($key) use ($filters) {
	        return in_array($key, $filters);
	    },
	    ARRAY_FILTER_USE_KEY
	);

	$filtered['all'] = 'All';

	return $filtered;
}
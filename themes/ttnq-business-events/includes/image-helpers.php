<?php

// create custom hero image size
add_theme_support( 'post-thumbnails' );
add_image_size( 'hero-thumb', 100, 100, false );
add_image_size( 'mobile-banner', 640, 1280, true );

// convert image to base64
function base_64_convert($path) {

   $type = pathinfo($path, PATHINFO_EXTENSION);
   $data = file_get_contents($path);
   return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

// setup responsive image feature image
function responsive_image_post($image_post_size){

	global $post; 

	$image_id = get_post_thumbnail_id($post->ID);
	// set the default src image size
	$image_src = wp_get_attachment_image_url($image_id, $image_post_size);

	// set the srcset with various image sizes
	$image_srcset = wp_get_attachment_image_srcset($image_id, $image_post_size);

	// generate the markup for the responsive image
	echo 'src="'.$image_src.'" srcset="'.$image_srcset.'"';
}

// setup responsive image feature image
function responsive_image_post_return($image_post_size){

	global $post; 

	$image_id = get_post_thumbnail_id($post->ID);
	// set the default src image size
	$image_src = wp_get_attachment_image_url($image_id, $image_post_size);

	// set the srcset with various image sizes
	$image_srcset = wp_get_attachment_image_srcset($image_id, $image_post_size);

	// generate the markup for the responsive image
	return 'src="'.$image_src.'" srcset="'.$image_srcset.'"';
}

// setup responsive image from acf field
function responsive_image($image_id, $image_size){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);

		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" alt="'.$image_alt.'"';

	}
}

// return responsive from acf field.
// usefull when creating markup in php.
function responsive_image_return($image_id, $image_size){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);

		// generate the markup for the responsive image
		return 'src="'.$image_src.'" srcset="'.$image_srcset.'" alt="'.$image_alt.'"';

	}
}

// Allow svg
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


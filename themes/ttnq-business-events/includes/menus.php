<?php

// Add Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
    	'main-menu' => __( 'Main Menu' ), 
    	'footer-legal-menu' => __( 'Footer Legal Menu' ),
    	'our-region-menu' => __( 'Our Region Menu' ),
    	'suppliers-menu' => __( 'Suppliers Menu' ),
    	'media-menu' => __( 'Media Menu' ),
    	'events-menu' => __( 'Events Menu' ),
    	'resources-menu' => __( 'Resources Menu' )
    )
  );
} 
add_action( 'init', 'register_my_menus' );

// add wrapper to sub menu for main menu
class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);

		if ($depth == 0) {
			$output .= "\n$indent<div class='sub-menu-wrapper'><div class='clearfix'><ul class='sub-menu'>\n";
		} else {
			$output .= "\n$indent<ul class='sub-menu-child'>\n";
		}
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);

		if ($depth == 0) {
			$output .= "$indent</ul></div></div>\n";
		} else {
			$output .= "$indent</ul>\n";
		}
	}
}
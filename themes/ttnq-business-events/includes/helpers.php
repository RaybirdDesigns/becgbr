<?php

/*
	=======================================
	Standardise urls. Add http if needed.
	=======================================
*/
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/*
	=======================================
	Generate random string.
	=======================================
*/
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


/*
    =======================================
    Generate random string.
    =======================================
*/
function hexToRGB($hex) {
    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
    return $r.",".$g.",".$b;
}


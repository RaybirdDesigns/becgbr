<?php

function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Accommodation Types.
	 */

	$labels = array(
		"name" => __( "Accommodation Types", "" ),
		"singular_name" => __( "Accommodation Type", "" ),
		"menu_name" => __( "Accommodation Types", "" ),
		"all_items" => __( "All Accommodation Types", "" ),
		"edit_item" => __( "Edit Accommodation Types", "" ),
		"view_item" => __( "View Accommodation Types", "" ),
		"update_item" => __( "Update Accommodation Types", "" ),
		"add_new_item" => __( "Add New Accommodation Types", "" ),
		"new_item_name" => __( "New Accommodation Types", "" ),
	);

	$args = array(
		"label" => __( "Accommodation Types", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Accommodation Types",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'accommodation_type', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "accommodation_type",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "accommodation_type", array( "accommodation" ), $args );

	/**
	 * Taxonomy: Activities Type.
	 */

	$labels = array(
		"name" => __( "Activities Type", "" ),
		"singular_name" => __( "Activity Type", "" ),
		"menu_name" => __( "Activity Type", "" ),
		"all_items" => __( "All Activity Types", "" ),
		"edit_item" => __( "Edit Activity Type", "" ),
		"view_item" => __( "View Activity Type", "" ),
		"update_item" => __( "Update Activity Type", "" ),
		"add_new_item" => __( "Add New Activity Type", "" ),
		"new_item_name" => __( "New Activity Type", "" ),
	);

	$args = array(
		"label" => __( "Activities Type", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Activities Type",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'activity_type', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "activity_type",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "activity_type", array( "activities" ), $args );

	/**
	 * Taxonomy: Support Types.
	 */

	$labels = array(
		"name" => __( "Support Types", "" ),
		"singular_name" => __( "Support Type", "" ),
		"menu_name" => __( "Support Types", "" ),
		"all_items" => __( "All Support Types", "" ),
		"edit_item" => __( "Edit Support Types", "" ),
		"view_item" => __( "View Support Types", "" ),
		"update_item" => __( "Update Support Types", "" ),
		"add_new_item" => __( "Add New Support Types", "" ),
		"new_item_name" => __( "New Support Types", "" ),
	);

	$args = array(
		"label" => __( "Support Types", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Support Types",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'support_type', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "support_type",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "support_type", array( "professional_support" ), $args );

	/**
	 * Taxonomy: Conference Types.
	 */

	$labels = array(
		"name" => __( "Conference Types", "" ),
		"singular_name" => __( "Conference Type", "" ),
		"menu_name" => __( "Conference Types", "" ),
		"all_items" => __( "All Conference Types", "" ),
		"edit_item" => __( "Edit Conference Types", "" ),
		"view_item" => __( "View Conference Types", "" ),
		"update_item" => __( "Update Conference Types", "" ),
		"add_new_item" => __( "Add New Conference Types", "" ),
		"new_item_name" => __( "New Conference Types", "" ),
	);

	$args = array(
		"label" => __( "Conference Types", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Conference Types",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'conference_type', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "conference_type",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "conference_type", array( "conference_venue" ), $args );

	/**
	 * Taxonomy: Dining & Restaurant Types.
	 */

	$labels = array(
		"name" => __( "Dining & Restaurant Types", "" ),
		"singular_name" => __( "Dining & Restaurant Type", "" ),
		"menu_name" => __( "Dining & Restaurant Types", "" ),
		"all_items" => __( "All Dining & Restaurant Types", "" ),
		"edit_item" => __( "Edit Dining & Restaurant Types", "" ),
		"view_item" => __( "View Dining & Restaurant Types", "" ),
		"update_item" => __( "Update Dining & Restaurant Types", "" ),
		"add_new_item" => __( "Add New Dining & Restaurant Types", "" ),
		"new_item_name" => __( "New Dining & Restaurant Types", "" ),
	);

	$args = array(
		"label" => __( "Dining & Restaurant Types", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Dining & Restaurant Types",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'dining_restaurant_types', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "dining_restaurant_types",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "dining_restaurant_types", array( "dining_restaurants" ), $args );

	/**
	 * Taxonomy: Off Site Venue Types.
	 */

	$labels = array(
		"name" => __( "Off Site Venue Types", "" ),
		"singular_name" => __( "Off Site Venue Type", "" ),
		"menu_name" => __( "Off Site Venue Types", "" ),
		"all_items" => __( "All Off Site Venue Types", "" ),
		"edit_item" => __( "Edit Off Site Venue Types", "" ),
		"view_item" => __( "View Off Site Venue Types", "" ),
		"update_item" => __( "Update Off Site Venue Types", "" ),
		"add_new_item" => __( "Add New Off Site Venue Types", "" ),
		"new_item_name" => __( "New Off Site Venue Types", "" ),
	);

	$args = array(
		"label" => __( "Off Site Venue Types", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Off Site Venue Types",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'off_site_venue_types', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "off_site_venue_types",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "off_site_venue_types", array( "offsite_venues" ), $args );

	/**
	 * Taxonomy: Sights.
	 */

	$labels = array(
		"name" => __( "Sights", "" ),
		"singular_name" => __( "Sight", "" ),
		"menu_name" => __( "Sights", "" ),
		"all_items" => __( "All Sights", "" ),
		"edit_item" => __( "Edit Sights", "" ),
		"view_item" => __( "View Sight", "" ),
		"update_item" => __( "Update Sight", "" ),
		"add_new_item" => __( "Add New Sight", "" ),
	);

	$args = array(
		"label" => __( "Sights", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Sights",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'sights', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "sights",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "sights", array( "itinerary" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes' );

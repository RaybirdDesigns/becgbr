@font-face {
    font-family: 'open_sansbold';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-bold-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-bold-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'open_sansextrabold';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-extrabold-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-extrabold-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'open_sanslight';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-light-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-light-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'open_sanslight_italic';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-lightitalic-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-lightitalic-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'open_sansregular';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-regular-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensans-regular-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'open_sans_condensedbold';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensanscondensed-bold-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensanscondensed-bold-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'open_sans_hebrew_condensedRg';
    src: url('<?php echo get_template_directory_uri(); ?>/fonts/opensanshebrewcondensed-regular-webfont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri(); ?>/fonts/opensanshebrewcondensed-regular-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}
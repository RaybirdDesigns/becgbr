<?php 

// Options page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Global Content and Settings',
		'menu_title'	=> 'Global Content',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

add_filter('acf/settings/google_api_key', function () {
    return 'AIzaSyCjbF6HD_APAIcm_KenNIKYwNQuwpcgx3E';
});
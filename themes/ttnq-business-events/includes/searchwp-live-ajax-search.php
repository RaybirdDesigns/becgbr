<?php


// authentication for stage
function my_searchwp_basic_auth_creds() {
	
	$credentials = array( 
		'username' => 'content-team', // the HTTP BASIC AUTH username
		'password' => 'r33fr33f2017'  // the HTTP BASIC AUTH password
	);
	
	return $credentials;
}

// stage only!
// if (array_shift((explode('.', $_SERVER['HTTP_HOST']))) == 'staging') {
// 	add_filter( 'searchwp_basic_auth_creds', 'my_searchwp_basic_auth_creds' );
// }

// settings
function my_searchwp_live_search_configs( $configs ) {
	// override some defaults
	$configs['default'] = array(
		'engine' => 'default',                  // search engine to use (if SearchWP is available)
		'input' => array(
			'delay'     => 300,                 // wait 500ms before triggering a search
			'min_chars' => 3,                   // wait for at least 3 characters before triggering a search
		),
		'results' => array(
			'position'  => 'bottom',            // where to position the results (bottom|top)
			'width'     => 'auto',              // whether the width should automatically match the input (auto|css)
			'offset'    => array(
				'x' => 0,                       // x offset (in pixels)
				'y' => 5                        // y offset (in pixels)
			),
		),
		'spinner' => array(                    // powered by http://fgnass.github.io/spin.js/
			'lines'         => 9,              // number of lines in the spinner
			'length'        => 0,               // length of each line
			'width'         => 6,               // line thickness
			'radius'        => 8,
			'scale'			=> 2.75,               // radius of inner circle
			'corners'       => 1,               // corner roundness (0..1)
			'rotate'        => 0,               // rotation offset
			'direction'     => 1,               // 1: clockwise, -1: counterclockwise
			'color'         => '#b5b5b5',          // #rgb or #rrggbb or array of colors
			'speed'         => 1.3,               // rounds per second
			'trail'         => 100,              // afterglow percentage
			'shadow'        => false,           // whether to render a shadow
			'hwaccel'       => false,           // whether to use hardware acceleration
			'className'     => 'spinner',       // CSS class assigned to spinner
			'zIndex'        => 2000000000,      // z-index of spinner
			'top'           => '50%',           // top position (relative to parent)
			'left'          => '50%',           // left position (relative to parent)
		),
	);
	
	return $configs;
}

add_filter( 'searchwp_live_search_configs', 'my_searchwp_live_search_configs' );

// set spell check for fuzzy matches
function my_fuzzy_threshold()
{
  return 50;
}
add_filter( 'searchwp_fuzzy_threshold', 'my_fuzzy_threshold' );

// fuzzy word length thresold
function my_fuzzy_word_length()
{
  return 4;
}
add_filter( 'searchwp_fuzzy_min_length', 'my_fuzzy_word_length' );

// Remove default styles for ajax search
function my_remove_searchwp_live_search_theme_css() {
	wp_dequeue_style( 'searchwp-live-search' );
	// wp_dequeue_script( 'swp-live-search-client' );
}
add_action( 'wp_enqueue_scripts', 'my_remove_searchwp_live_search_theme_css', 100 );

// remove base positioning
// add_filter( 'searchwp_live_search_base_styles', '__return_false' );

// control number of results returned
function my_searchwp_live_search_posts_per_page() {
	return 3;
}
add_filter( 'searchwp_live_search_posts_per_page', 'my_searchwp_live_search_posts_per_page' );
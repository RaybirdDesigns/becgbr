<?php

function cptui_register_my_cpts() {

	/**
	 * Post Type: Accommodation.
	 */

	$labels = array(
		"name" => __( "Accommodation", "" ),
		"singular_name" => __( "Accommodation", "" ),
		"menu_name" => __( "Accommodation", "" ),
		"all_items" => __( "All Accommodation", "" ),
		"add_new" => __( "Add New Accommodation", "" ),
		"add_new_item" => __( "Add New Accommodation", "" ),
		"edit_item" => __( "Edit Accommodation", "" ),
		"new_item" => __( "New Accommodation", "" ),
		"view_item" => __( "View Accommodation", "" ),
		"view_items" => __( "View Accommodation", "" ),
		"search_items" => __( "Searc Accommodation", "" ),
		"not_found" => __( "No Accommodation Found", "" ),
		"featured_image" => __( "Featured image for this accommodation", "" ),
		"set_featured_image" => __( "Set feature image for this accommodation", "" ),
		"remove_featured_image" => __( "Remove feature image for this accommodation", "" ),
		"use_featured_image" => __( "Use feature image for this accommodation", "" ),
		"archives" => __( "Accommodation archives", "" ),
	);

	$args = array(
		"label" => __( "Accommodation", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "accommodation", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-store",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "accommodation", $args );

	/**
	 * Post Type: Off Site Venues.
	 */

	$labels = array(
		"name" => __( "Offsite Venues", "" ),
		"singular_name" => __( "Offsite Venue", "" ),
		"menu_name" => __( "Offsite Venues", "" ),
		"all_items" => __( "All Offsite Venues", "" ),
		"add_new" => __( "Add New Offsite Venue", "" ),
		"add_new_item" => __( "Add Off Site Venue", "" ),
		"edit_item" => __( "Edit Offsite Venue", "" ),
		"new_item" => __( "New Offsite Venue", "" ),
		"view_item" => __( "View Offsite Venue", "" ),
		"view_items" => __( "View Offsite Venues", "" ),
		"search_items" => __( "Search Offsite Venue", "" ),
		"not_found" => __( "No Off Site Venues Found", "" ),
		"featured_image" => __( "Featured image for this offsite venue", "" ),
		"set_featured_image" => __( "Set feature image for this offsite venue", "" ),
		"remove_featured_image" => __( "Remove feature image for this offsite venue", "" ),
		"use_featured_image" => __( "Use feature image for this offsite venue", "" ),
		"archives" => __( "Offsite venue archives", "" ),
	);

	$args = array(
		"label" => __( "Offsite Venues", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "offsite-venues", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-admin-site",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "offsite_venues", $args );

	/**
	 * Post Type: Dining & Restaurants.
	 */

	$labels = array(
		"name" => __( "Dining & Restaurants", "" ),
		"singular_name" => __( "Dining & Restaurant", "" ),
		"menu_name" => __( "Dining & Restaurants", "" ),
		"all_items" => __( "All Dining & Restaurants", "" ),
		"add_new" => __( "Add New Dining & Restaurants", "" ),
		"add_new_item" => __( "Add new Dining & Restaurant", "" ),
		"edit_item" => __( "Edit Dining & Restaurants", "" ),
		"new_item" => __( "new Dining & Restaurants", "" ),
		"view_item" => __( "View Dining & Restaurant", "" ),
		"view_items" => __( "View Dining & Restaurants", "" ),
		"search_items" => __( "Search Dining & Restaurants", "" ),
		"not_found" => __( "No Dining & Restaurants Found", "" ),
	);

	$args = array(
		"label" => __( "Dining & Restaurants", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "dining-restaurants", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-carrot",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "dining_restaurants", $args );

	/**
	 * Post Type: Conference Venues.
	 */

	$labels = array(
		"name" => __( "Conference/ Meeting Venues", "" ),
		"singular_name" => __( "Conference/ Meeting Venue", "" ),
		"menu_name" => __( "Conference/ Meeting Venues", "" ),
		"all_items" => __( "All Conference/ Meeting Venues", "" ),
		"add_new" => __( "Add New Conference/ Meeting Venues", "" ),
		"add_new_item" => __( "Add New Conference/ Meeting Venue", "" ),
		"edit_item" => __( "Edit Conference/ Meeting Venue", "" ),
		"new_item" => __( "New Conference/ Meeting Venue", "" ),
		"view_item" => __( "View Conference/ Meeting Venue", "" ),
		"view_items" => __( "View Conference/ Meeting Venues", "" ),
		"search_items" => __( "Search Conference/ Meeting Venues", "" ),
		"not_found" => __( "No Conference/ Meeting Venues Found", "" ),
	);

	$args = array(
		"label" => __( "Conference/ Meeting Venues", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "conference-venue", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-building",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "conference_venue", $args );

	/**
	 * Post Type: Activities and Team Building.
	 */

	$labels = array(
		"name" => __( "Activities and Team Building", "" ),
		"singular_name" => __( "Activity and Team Building", "" ),
		"menu_name" => __( "Activities and Team Building", "" ),
		"all_items" => __( "All Activities and Team Building", "" ),
		"add_new" => __( "Add Activities and Team Building", "" ),
		"add_new_item" => __( "Activity and Team Building", "" ),
		"edit_item" => __( "Edit Activities and Team Building", "" ),
		"new_item" => __( "New Activities and Team Building", "" ),
		"view_item" => __( "View Activity and Team Building", "" ),
		"view_items" => __( "View Activities and Team Building", "" ),
		"search_items" => __( "Search Activities and Team Building", "" ),
		"not_found" => __( "No Activities and Team Building Found", "" ),
	);

	$args = array(
		"label" => __( "Activities and Team Building", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "activities", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-groups",
		"supports" => array( "title", "editor", "thumbnail", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "activities", $args );

	/**
	 * Post Type: Professional Support.
	 */

	$labels = array(
		"name" => __( "Professional Support", "" ),
		"singular_name" => __( "Professional Support", "" ),
		"menu_name" => __( "Professional Support", "" ),
		"all_items" => __( "All Professional Support", "" ),
		"add_new" => __( "Add Professional Support", "" ),
		"add_new_item" => __( "Add New Professional Support", "" ),
		"edit_item" => __( "Edit Professional Support", "" ),
		"new_item" => __( "New Professional Support", "" ),
		"view_item" => __( "View Professional Support", "" ),
		"view_items" => __( "View Professional Support", "" ),
		"search_items" => __( "Search Professional Support", "" ),
		"not_found" => __( "No Professional Support Found", "" ),
	);

	$args = array(
		"label" => __( "Professional Support", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "professional-support", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-sos",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "professional_support", $args );

	/**
	 * Post Type: Itineraries.
	 */

	$labels = array(
		"name" => __( "Itineraries", "" ),
		"singular_name" => __( "Itinerary", "" ),
		"menu_name" => __( "Itineraries", "" ),
		"all_items" => __( "All Itineraries", "" ),
		"add_new" => __( "Add itinerary", "" ),
		"add_new_item" => __( "Add New Itinerary", "" ),
		"edit_item" => __( "Edit Itinerary", "" ),
		"new_item" => __( "New Itinerary", "" ),
		"view_item" => __( "View Itinerary", "" ),
		"view_items" => __( "View Itineraries", "" ),
		"search_items" => __( "Search Itineraries", "" ),
		"not_found" => __( "No Itineraries Found", "" ),
	);

	$args = array(
		"label" => __( "Itineraries", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "Itineraries", "with_front" => false ),
		"query_var" => true,
		"menu_icon" => "dashicons-location-alt",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "author" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "itinerary", $args );

	/**
	 * Post Type: Ad Units.
	 */

	$labels = array(
		"name" => __( "Ad Units", "" ),
		"singular_name" => __( "Ad Unit", "" ),
		"menu_name" => __( "Ad Unit", "" ),
		"all_items" => __( "All Ad Units", "" ),
		"add_new" => __( "Add New Ad Unit", "" ),
		"add_new_item" => __( "Add New Ad Unit", "" ),
	);

	$args = array(
		"label" => __( "Ad Units", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "ad_unit", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-money",
		"supports" => array( "title" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "ad_unit", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

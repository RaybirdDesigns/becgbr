<?php
/**
 * Serve theme styles via a hashed filename instead of WordPress' default style.css.
 *
 * Checks for a hashed filename as a value in a JSON object.
 * If it exists: the hashed filename is enqueued in place of style.css.
 * Fallback: the default style.css will be passed through.
 *
 * @param string $css is WordPress’ required, known location for CSS: style.css
 */

function get_hashed_asset( $file_name ) {

    $map = get_template_directory() . '/build/asset-manifest.json';
    static $hash = null;

    if ( null === $hash ) {
        $hash = file_exists( $map ) ? json_decode( file_get_contents( $map ), true ) : [];
    }

    if ( array_key_exists( $file_name, $hash )) {
    	if (base_url() == 'http://localhost:8080') {
    		return '/build/' . $file_name;
    	} else {
        	return '/build/' . str_replace('../build/', '', $hash[ $file_name ]);
    	}
    }

    return '/build/'.$file_name;

}
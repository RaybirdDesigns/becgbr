<form role="search" method="get" class="search-form" action="<?php echo site_url(); ?>">
	<label>
		<span class="show-for-sr">Search for:</span>
		<input type="search" class="search-field" placeholder="Enter search terms..." value="" name="s" title="Search for:" />
	</label>
</form>